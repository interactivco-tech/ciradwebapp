<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalSpecialtiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->string('image_prec_befor_exams')->nullable();
            $table->string('advice1_prec_befor_exams')->nullable();
            $table->string('advice2_prec_befor_exams')->nullable();
            $table->string('advice3_prec_befor_exams')->nullable();
            $table->string('advice4_prec_befor_exams')->nullable();
            $table->string('message_prec_befor_exams')->nullable();
            $table->text('description_prec_befor_exams')->nullable();
            $table->string('image_prec_during_exams')->nullable();
            $table->string('advice1_prec_during_exams')->nullable();
            $table->string('advice2_prec_during_exams')->nullable();
            $table->string('advice3_prec_during_exams')->nullable();
            $table->string('advice4_prec_during_exams')->nullable();
            $table->string('message_prec_during_exams')->nullable();
            $table->text('description_prec_during_exams')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_specialties');
    }
}
