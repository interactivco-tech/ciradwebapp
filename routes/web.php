<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Artisan::call('storage:link');

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\FrontOfficeController::class, 'indexFrontOffice']);

Route::get('/contacter-nous', [App\Http\Controllers\FrontOfficeController::class, 'nousContacter'])->name('contacter-nous');

Route::get('/blog', [App\Http\Controllers\FrontOfficeController::class, 'indexBlog'])->name('blog');

Route::get('/detailblog/{slug}', [App\Http\Controllers\FrontOfficeController::class, "showBlog"]);

Route::get('/a-propos', [App\Http\Controllers\FrontOfficeController::class, 'indexAbout'])->name('a-propos');

Route::get('/prise-de-rendez-vous', [App\Http\Controllers\FrontOfficeController::class, 'appointmentPages'])->name('prise-de-rendez-vous');

Route::get('/irm', [App\Http\Controllers\FrontOfficeController::class, 'irmPage'])->name('irm');

Route::get('/biopsie', [App\Http\Controllers\FrontOfficeController::class, 'biopsiePage'])->name('biopsie');

Route::get('/echographie', [App\Http\Controllers\FrontOfficeController::class, 'echographiePage'])->name('echographie');

Route::get('/scanner', [App\Http\Controllers\FrontOfficeController::class, 'scannerPage'])->name('scanner');

Route::get('/mammographie', [App\Http\Controllers\FrontOfficeController::class, 'mammographiePage'])->name('mammographie');

Route::get('/radiographie', [App\Http\Controllers\FrontOfficeController::class, 'radiographiePage'])->name('radiographie');

Route::get('/bilan-sante', [App\Http\Controllers\FrontOfficeController::class, 'healhCheckPage'])->name('bilan-sante');

Route::get('/analyse-mediacale', [App\Http\Controllers\FrontOfficeController::class, 'medicalAnalysPage'])->name('analyse-mediacale');

Route::post('/footer', [App\Http\Controllers\NewslettersController::class, "addEmail"])->name('footer');

Route::post('/appointmentess', [App\Http\Controllers\AppointmentController::class, "SendAppointment"])->name('appointmentess');

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('slides', App\Http\Controllers\SlideController::class);

Route::resource('medicalSpecialties', App\Http\Controllers\MedicalSpecialtiesController::class);

Route::resource('appointments', App\Http\Controllers\AppointmentController::class);

Route::resource('blogs', App\Http\Controllers\BlogController::class);

Route::resource('newsletters', App\Http\Controllers\NewslettersController::class);

Route::resource('testimonials', App\Http\Controllers\TestimonialsController::class);

Route::resource('medicalCheckups', App\Http\Controllers\MedicalCheckupsController::class);

Route::resource('videoStores', App\Http\Controllers\VideoStoresController::class);

Route::post('ckeditor/upload', [App\Http\Controllers\CKEditorController::class, "upload"])->name('upload');
