<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('slides', App\Http\Controllers\API\SlideAPIController::class);

Route::resource('medical_specialties', App\Http\Controllers\API\MedicalSpecialtiesAPIController::class);

Route::resource('appointments', App\Http\Controllers\API\AppointmentAPIController::class);

Route::resource('blogs', App\Http\Controllers\API\BlogAPIController::class);

Route::resource('newsletters', App\Http\Controllers\API\NewslettersAPIController::class);

Route::resource('testimonials', App\Http\Controllers\API\TestimonialsAPIController::class);

Route::resource('medical_checkups', App\Http\Controllers\API\MedicalCheckupsAPIController::class);

Route::resource('video_stores', App\Http\Controllers\API\VideoStoresAPIController::class);
