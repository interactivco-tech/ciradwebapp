var myPaginator = new $('.items-container').joldPaginator({

    // the number of items per page
    'perPage': 5,

    // selectors
    'items': '.item',
    'paginator': '.pagination-container',

    // custom indicator
    'indicator': {
      'selector': '.pagination-indicator',
      'text': 'Showing item {start}-{end} of {total}',
    }
    
});

myPaginator.init();