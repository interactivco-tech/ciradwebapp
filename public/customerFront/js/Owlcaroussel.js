$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 0,
        responsiveClass: true,
        autoplay:true,
        responsive: {
            0: {
                items: 1,
                nav: true,
                autoHeight: true
            },
            400: {
                items: 1,
                nav: true,
                autoHeight: true
            },
            600: {
                items: 1,
                nav: true,
                autoHeight: true
            },
            1000: {
                items: 1,
                nav: false,
                loop: true,
                autoplay: true,
                
            }
        }
    })
});