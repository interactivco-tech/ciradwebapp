<?php namespace Tests\Repositories;

use App\Models\PrecautionBeforExams;
use App\Repositories\PrecautionBeforExamsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PrecautionBeforExamsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PrecautionBeforExamsRepository
     */
    protected $precautionBeforExamsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->precautionBeforExamsRepo = \App::make(PrecautionBeforExamsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->make()->toArray();

        $createdPrecautionBeforExams = $this->precautionBeforExamsRepo->create($precautionBeforExams);

        $createdPrecautionBeforExams = $createdPrecautionBeforExams->toArray();
        $this->assertArrayHasKey('id', $createdPrecautionBeforExams);
        $this->assertNotNull($createdPrecautionBeforExams['id'], 'Created PrecautionBeforExams must have id specified');
        $this->assertNotNull(PrecautionBeforExams::find($createdPrecautionBeforExams['id']), 'PrecautionBeforExams with given id must be in DB');
        $this->assertModelData($precautionBeforExams, $createdPrecautionBeforExams);
    }

    /**
     * @test read
     */
    public function test_read_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();

        $dbPrecautionBeforExams = $this->precautionBeforExamsRepo->find($precautionBeforExams->id);

        $dbPrecautionBeforExams = $dbPrecautionBeforExams->toArray();
        $this->assertModelData($precautionBeforExams->toArray(), $dbPrecautionBeforExams);
    }

    /**
     * @test update
     */
    public function test_update_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();
        $fakePrecautionBeforExams = PrecautionBeforExams::factory()->make()->toArray();

        $updatedPrecautionBeforExams = $this->precautionBeforExamsRepo->update($fakePrecautionBeforExams, $precautionBeforExams->id);

        $this->assertModelData($fakePrecautionBeforExams, $updatedPrecautionBeforExams->toArray());
        $dbPrecautionBeforExams = $this->precautionBeforExamsRepo->find($precautionBeforExams->id);
        $this->assertModelData($fakePrecautionBeforExams, $dbPrecautionBeforExams->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();

        $resp = $this->precautionBeforExamsRepo->delete($precautionBeforExams->id);

        $this->assertTrue($resp);
        $this->assertNull(PrecautionBeforExams::find($precautionBeforExams->id), 'PrecautionBeforExams should not exist in DB');
    }
}
