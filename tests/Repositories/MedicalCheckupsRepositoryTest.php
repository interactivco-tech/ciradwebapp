<?php namespace Tests\Repositories;

use App\Models\MedicalCheckups;
use App\Repositories\MedicalCheckupsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicalCheckupsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicalCheckupsRepository
     */
    protected $medicalCheckupsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicalCheckupsRepo = \App::make(MedicalCheckupsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->make()->toArray();

        $createdMedicalCheckups = $this->medicalCheckupsRepo->create($medicalCheckups);

        $createdMedicalCheckups = $createdMedicalCheckups->toArray();
        $this->assertArrayHasKey('id', $createdMedicalCheckups);
        $this->assertNotNull($createdMedicalCheckups['id'], 'Created MedicalCheckups must have id specified');
        $this->assertNotNull(MedicalCheckups::find($createdMedicalCheckups['id']), 'MedicalCheckups with given id must be in DB');
        $this->assertModelData($medicalCheckups, $createdMedicalCheckups);
    }

    /**
     * @test read
     */
    public function test_read_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();

        $dbMedicalCheckups = $this->medicalCheckupsRepo->find($medicalCheckups->id);

        $dbMedicalCheckups = $dbMedicalCheckups->toArray();
        $this->assertModelData($medicalCheckups->toArray(), $dbMedicalCheckups);
    }

    /**
     * @test update
     */
    public function test_update_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();
        $fakeMedicalCheckups = MedicalCheckups::factory()->make()->toArray();

        $updatedMedicalCheckups = $this->medicalCheckupsRepo->update($fakeMedicalCheckups, $medicalCheckups->id);

        $this->assertModelData($fakeMedicalCheckups, $updatedMedicalCheckups->toArray());
        $dbMedicalCheckups = $this->medicalCheckupsRepo->find($medicalCheckups->id);
        $this->assertModelData($fakeMedicalCheckups, $dbMedicalCheckups->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();

        $resp = $this->medicalCheckupsRepo->delete($medicalCheckups->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicalCheckups::find($medicalCheckups->id), 'MedicalCheckups should not exist in DB');
    }
}
