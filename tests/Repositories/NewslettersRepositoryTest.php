<?php namespace Tests\Repositories;

use App\Models\Newsletters;
use App\Repositories\NewslettersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NewslettersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NewslettersRepository
     */
    protected $newslettersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->newslettersRepo = \App::make(NewslettersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_newsletters()
    {
        $newsletters = Newsletters::factory()->make()->toArray();

        $createdNewsletters = $this->newslettersRepo->create($newsletters);

        $createdNewsletters = $createdNewsletters->toArray();
        $this->assertArrayHasKey('id', $createdNewsletters);
        $this->assertNotNull($createdNewsletters['id'], 'Created Newsletters must have id specified');
        $this->assertNotNull(Newsletters::find($createdNewsletters['id']), 'Newsletters with given id must be in DB');
        $this->assertModelData($newsletters, $createdNewsletters);
    }

    /**
     * @test read
     */
    public function test_read_newsletters()
    {
        $newsletters = Newsletters::factory()->create();

        $dbNewsletters = $this->newslettersRepo->find($newsletters->id);

        $dbNewsletters = $dbNewsletters->toArray();
        $this->assertModelData($newsletters->toArray(), $dbNewsletters);
    }

    /**
     * @test update
     */
    public function test_update_newsletters()
    {
        $newsletters = Newsletters::factory()->create();
        $fakeNewsletters = Newsletters::factory()->make()->toArray();

        $updatedNewsletters = $this->newslettersRepo->update($fakeNewsletters, $newsletters->id);

        $this->assertModelData($fakeNewsletters, $updatedNewsletters->toArray());
        $dbNewsletters = $this->newslettersRepo->find($newsletters->id);
        $this->assertModelData($fakeNewsletters, $dbNewsletters->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_newsletters()
    {
        $newsletters = Newsletters::factory()->create();

        $resp = $this->newslettersRepo->delete($newsletters->id);

        $this->assertTrue($resp);
        $this->assertNull(Newsletters::find($newsletters->id), 'Newsletters should not exist in DB');
    }
}
