<?php namespace Tests\Repositories;

use App\Models\PrecautioDuringExams;
use App\Repositories\PrecautioDuringExamsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PrecautioDuringExamsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PrecautioDuringExamsRepository
     */
    protected $precautioDuringExamsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->precautioDuringExamsRepo = \App::make(PrecautioDuringExamsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->make()->toArray();

        $createdPrecautioDuringExams = $this->precautioDuringExamsRepo->create($precautioDuringExams);

        $createdPrecautioDuringExams = $createdPrecautioDuringExams->toArray();
        $this->assertArrayHasKey('id', $createdPrecautioDuringExams);
        $this->assertNotNull($createdPrecautioDuringExams['id'], 'Created PrecautioDuringExams must have id specified');
        $this->assertNotNull(PrecautioDuringExams::find($createdPrecautioDuringExams['id']), 'PrecautioDuringExams with given id must be in DB');
        $this->assertModelData($precautioDuringExams, $createdPrecautioDuringExams);
    }

    /**
     * @test read
     */
    public function test_read_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();

        $dbPrecautioDuringExams = $this->precautioDuringExamsRepo->find($precautioDuringExams->id);

        $dbPrecautioDuringExams = $dbPrecautioDuringExams->toArray();
        $this->assertModelData($precautioDuringExams->toArray(), $dbPrecautioDuringExams);
    }

    /**
     * @test update
     */
    public function test_update_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();
        $fakePrecautioDuringExams = PrecautioDuringExams::factory()->make()->toArray();

        $updatedPrecautioDuringExams = $this->precautioDuringExamsRepo->update($fakePrecautioDuringExams, $precautioDuringExams->id);

        $this->assertModelData($fakePrecautioDuringExams, $updatedPrecautioDuringExams->toArray());
        $dbPrecautioDuringExams = $this->precautioDuringExamsRepo->find($precautioDuringExams->id);
        $this->assertModelData($fakePrecautioDuringExams, $dbPrecautioDuringExams->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();

        $resp = $this->precautioDuringExamsRepo->delete($precautioDuringExams->id);

        $this->assertTrue($resp);
        $this->assertNull(PrecautioDuringExams::find($precautioDuringExams->id), 'PrecautioDuringExams should not exist in DB');
    }
}
