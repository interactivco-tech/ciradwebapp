<?php namespace Tests\Repositories;

use App\Models\MedicalSpecialties;
use App\Repositories\MedicalSpecialtiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicalSpecialtiesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicalSpecialtiesRepository
     */
    protected $medicalSpecialtiesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicalSpecialtiesRepo = \App::make(MedicalSpecialtiesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->make()->toArray();

        $createdMedicalSpecialties = $this->medicalSpecialtiesRepo->create($medicalSpecialties);

        $createdMedicalSpecialties = $createdMedicalSpecialties->toArray();
        $this->assertArrayHasKey('id', $createdMedicalSpecialties);
        $this->assertNotNull($createdMedicalSpecialties['id'], 'Created MedicalSpecialties must have id specified');
        $this->assertNotNull(MedicalSpecialties::find($createdMedicalSpecialties['id']), 'MedicalSpecialties with given id must be in DB');
        $this->assertModelData($medicalSpecialties, $createdMedicalSpecialties);
    }

    /**
     * @test read
     */
    public function test_read_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();

        $dbMedicalSpecialties = $this->medicalSpecialtiesRepo->find($medicalSpecialties->id);

        $dbMedicalSpecialties = $dbMedicalSpecialties->toArray();
        $this->assertModelData($medicalSpecialties->toArray(), $dbMedicalSpecialties);
    }

    /**
     * @test update
     */
    public function test_update_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();
        $fakeMedicalSpecialties = MedicalSpecialties::factory()->make()->toArray();

        $updatedMedicalSpecialties = $this->medicalSpecialtiesRepo->update($fakeMedicalSpecialties, $medicalSpecialties->id);

        $this->assertModelData($fakeMedicalSpecialties, $updatedMedicalSpecialties->toArray());
        $dbMedicalSpecialties = $this->medicalSpecialtiesRepo->find($medicalSpecialties->id);
        $this->assertModelData($fakeMedicalSpecialties, $dbMedicalSpecialties->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();

        $resp = $this->medicalSpecialtiesRepo->delete($medicalSpecialties->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicalSpecialties::find($medicalSpecialties->id), 'MedicalSpecialties should not exist in DB');
    }
}
