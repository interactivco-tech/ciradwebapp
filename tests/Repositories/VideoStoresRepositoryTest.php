<?php namespace Tests\Repositories;

use App\Models\VideoStores;
use App\Repositories\VideoStoresRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VideoStoresRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VideoStoresRepository
     */
    protected $videoStoresRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->videoStoresRepo = \App::make(VideoStoresRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_video_stores()
    {
        $videoStores = VideoStores::factory()->make()->toArray();

        $createdVideoStores = $this->videoStoresRepo->create($videoStores);

        $createdVideoStores = $createdVideoStores->toArray();
        $this->assertArrayHasKey('id', $createdVideoStores);
        $this->assertNotNull($createdVideoStores['id'], 'Created VideoStores must have id specified');
        $this->assertNotNull(VideoStores::find($createdVideoStores['id']), 'VideoStores with given id must be in DB');
        $this->assertModelData($videoStores, $createdVideoStores);
    }

    /**
     * @test read
     */
    public function test_read_video_stores()
    {
        $videoStores = VideoStores::factory()->create();

        $dbVideoStores = $this->videoStoresRepo->find($videoStores->id);

        $dbVideoStores = $dbVideoStores->toArray();
        $this->assertModelData($videoStores->toArray(), $dbVideoStores);
    }

    /**
     * @test update
     */
    public function test_update_video_stores()
    {
        $videoStores = VideoStores::factory()->create();
        $fakeVideoStores = VideoStores::factory()->make()->toArray();

        $updatedVideoStores = $this->videoStoresRepo->update($fakeVideoStores, $videoStores->id);

        $this->assertModelData($fakeVideoStores, $updatedVideoStores->toArray());
        $dbVideoStores = $this->videoStoresRepo->find($videoStores->id);
        $this->assertModelData($fakeVideoStores, $dbVideoStores->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_video_stores()
    {
        $videoStores = VideoStores::factory()->create();

        $resp = $this->videoStoresRepo->delete($videoStores->id);

        $this->assertTrue($resp);
        $this->assertNull(VideoStores::find($videoStores->id), 'VideoStores should not exist in DB');
    }
}
