<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PrecautioDuringExams;

class PrecautioDuringExamsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/precautio_during_exams', $precautioDuringExams
        );

        $this->assertApiResponse($precautioDuringExams);
    }

    /**
     * @test
     */
    public function test_read_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/precautio_during_exams/'.$precautioDuringExams->id
        );

        $this->assertApiResponse($precautioDuringExams->toArray());
    }

    /**
     * @test
     */
    public function test_update_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();
        $editedPrecautioDuringExams = PrecautioDuringExams::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/precautio_during_exams/'.$precautioDuringExams->id,
            $editedPrecautioDuringExams
        );

        $this->assertApiResponse($editedPrecautioDuringExams);
    }

    /**
     * @test
     */
    public function test_delete_precautio_during_exams()
    {
        $precautioDuringExams = PrecautioDuringExams::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/precautio_during_exams/'.$precautioDuringExams->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/precautio_during_exams/'.$precautioDuringExams->id
        );

        $this->response->assertStatus(404);
    }
}
