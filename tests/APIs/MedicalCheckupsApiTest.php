<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicalCheckups;

class MedicalCheckupsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medical_checkups', $medicalCheckups
        );

        $this->assertApiResponse($medicalCheckups);
    }

    /**
     * @test
     */
    public function test_read_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/medical_checkups/'.$medicalCheckups->id
        );

        $this->assertApiResponse($medicalCheckups->toArray());
    }

    /**
     * @test
     */
    public function test_update_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();
        $editedMedicalCheckups = MedicalCheckups::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medical_checkups/'.$medicalCheckups->id,
            $editedMedicalCheckups
        );

        $this->assertApiResponse($editedMedicalCheckups);
    }

    /**
     * @test
     */
    public function test_delete_medical_checkups()
    {
        $medicalCheckups = MedicalCheckups::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medical_checkups/'.$medicalCheckups->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medical_checkups/'.$medicalCheckups->id
        );

        $this->response->assertStatus(404);
    }
}
