<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\VideoStores;

class VideoStoresApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_video_stores()
    {
        $videoStores = VideoStores::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/video_stores', $videoStores
        );

        $this->assertApiResponse($videoStores);
    }

    /**
     * @test
     */
    public function test_read_video_stores()
    {
        $videoStores = VideoStores::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/video_stores/'.$videoStores->id
        );

        $this->assertApiResponse($videoStores->toArray());
    }

    /**
     * @test
     */
    public function test_update_video_stores()
    {
        $videoStores = VideoStores::factory()->create();
        $editedVideoStores = VideoStores::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/video_stores/'.$videoStores->id,
            $editedVideoStores
        );

        $this->assertApiResponse($editedVideoStores);
    }

    /**
     * @test
     */
    public function test_delete_video_stores()
    {
        $videoStores = VideoStores::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/video_stores/'.$videoStores->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/video_stores/'.$videoStores->id
        );

        $this->response->assertStatus(404);
    }
}
