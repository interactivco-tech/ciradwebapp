<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicalSpecialties;

class MedicalSpecialtiesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medical_specialties', $medicalSpecialties
        );

        $this->assertApiResponse($medicalSpecialties);
    }

    /**
     * @test
     */
    public function test_read_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/medical_specialties/'.$medicalSpecialties->id
        );

        $this->assertApiResponse($medicalSpecialties->toArray());
    }

    /**
     * @test
     */
    public function test_update_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();
        $editedMedicalSpecialties = MedicalSpecialties::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medical_specialties/'.$medicalSpecialties->id,
            $editedMedicalSpecialties
        );

        $this->assertApiResponse($editedMedicalSpecialties);
    }

    /**
     * @test
     */
    public function test_delete_medical_specialties()
    {
        $medicalSpecialties = MedicalSpecialties::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medical_specialties/'.$medicalSpecialties->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medical_specialties/'.$medicalSpecialties->id
        );

        $this->response->assertStatus(404);
    }
}
