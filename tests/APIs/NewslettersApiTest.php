<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Newsletters;

class NewslettersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_newsletters()
    {
        $newsletters = Newsletters::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/newsletters', $newsletters
        );

        $this->assertApiResponse($newsletters);
    }

    /**
     * @test
     */
    public function test_read_newsletters()
    {
        $newsletters = Newsletters::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/newsletters/'.$newsletters->id
        );

        $this->assertApiResponse($newsletters->toArray());
    }

    /**
     * @test
     */
    public function test_update_newsletters()
    {
        $newsletters = Newsletters::factory()->create();
        $editedNewsletters = Newsletters::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/newsletters/'.$newsletters->id,
            $editedNewsletters
        );

        $this->assertApiResponse($editedNewsletters);
    }

    /**
     * @test
     */
    public function test_delete_newsletters()
    {
        $newsletters = Newsletters::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/newsletters/'.$newsletters->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/newsletters/'.$newsletters->id
        );

        $this->response->assertStatus(404);
    }
}
