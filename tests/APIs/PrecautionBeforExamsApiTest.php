<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PrecautionBeforExams;

class PrecautionBeforExamsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/precaution_befor_exams', $precautionBeforExams
        );

        $this->assertApiResponse($precautionBeforExams);
    }

    /**
     * @test
     */
    public function test_read_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/precaution_befor_exams/'.$precautionBeforExams->id
        );

        $this->assertApiResponse($precautionBeforExams->toArray());
    }

    /**
     * @test
     */
    public function test_update_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();
        $editedPrecautionBeforExams = PrecautionBeforExams::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/precaution_befor_exams/'.$precautionBeforExams->id,
            $editedPrecautionBeforExams
        );

        $this->assertApiResponse($editedPrecautionBeforExams);
    }

    /**
     * @test
     */
    public function test_delete_precaution_befor_exams()
    {
        $precautionBeforExams = PrecautionBeforExams::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/precaution_befor_exams/'.$precautionBeforExams->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/precaution_befor_exams/'.$precautionBeforExams->id
        );

        $this->response->assertStatus(404);
    }
}
