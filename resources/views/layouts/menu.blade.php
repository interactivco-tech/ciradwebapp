<li class="nav-item">
    <a href="{{ route('slides.index') }}"
       class="nav-link {{ Request::is('slides*') ? 'active' : '' }}">
        <p>Slides</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('medicalSpecialties.index') }}"
       class="nav-link {{ Request::is('medicalSpecialties*') ? 'active' : '' }}">
        <p>Medical Specialties</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('appointments.index') }}"
       class="nav-link {{ Request::is('appointments*') ? 'active' : '' }}">
        <p>Appointments</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('blogs.index') }}"
       class="nav-link {{ Request::is('blogs*') ? 'active' : '' }}">
        <p>Blogs</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('newsletters.index') }}"
       class="nav-link {{ Request::is('newsletters*') ? 'active' : '' }}">
        <p>Newsletters</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('testimonials.index') }}"
       class="nav-link {{ Request::is('testimonials*') ? 'active' : '' }}">
        <p>Testimonials</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('medicalCheckups.index') }}"
       class="nav-link {{ Request::is('medicalCheckups*') ? 'active' : '' }}">
        <p>Medical Checkups</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('videoStores.index') }}"
       class="nav-link {{ Request::is('videoStores*') ? 'active' : '' }}">
        <p>Video Stores</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('precautionBeforExams.index') }}"
       class="nav-link {{ Request::is('precautionBeforExams*') ? 'active' : '' }}">
        <p>Precaution Befor Exams</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('precautioDuringExams.index') }}"
       class="nav-link {{ Request::is('precautioDuringExams*') ? 'active' : '' }}">
        <p>Precautio During Exams</p>
    </a>
</li>


