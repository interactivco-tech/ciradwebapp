<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre de l&rsquo;Examen:') !!}
            <p>{{ $medicalCheckups->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $medicalCheckups->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $medicalCheckups->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $medicalCheckups->updated_at }}</p>
        </div>
    </div>
</div>
