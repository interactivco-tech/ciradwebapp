<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('customer/img/brand.png') }}">

    <title>Prise de Rendez-vous sur le site CIRAD.CI</title>

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('customer/css/akwaba.css') }}">
    <style>
        :root {

            --main: #1c6a49;
            --secondary: #edaa0d;
            --secondary-dark: #cf940a;
        }

        html,
        body,
        .body {
            height: 100% !important;
            background-color: #ececec;
        }

        .body {
            max-width: 500px;
            text-align: left !important;
            margin: auto;
        }

        .text-primary {
            color: #1c6a49;
        }

        .btn {
            text-decoration: none;
            padding: 10px;
            background-color: #26a85a;
            border-radius: 3px;
            color: white;
        }

        .btn:hover {
            background-color: #31c96e
        }

        .card {
            position: absolute;
            top: 20%;
            transform: translate(0, -20%);
            max-width: 500px;
            background: white;
            padding: 20px;
            border-radius: 10px;
        }

        tr {
            border: none !important;
        }

    </style>
</head>

<body style="background: #ececec; padding: 50px;">
    <table class="body"
        style="position: absolute;top: 10%;transform: translate(0, -10%);max-width: 500px;background: white;padding: 20px;border-radius: 10px;">
        <tbody class="card bg-white">
            <tr>
                <td>
                    <h1 class="text-primary mt-2" style="color: #1c6a49;">
                        Votre Nouvelle demande de Prise de Rendez-vous
                    </h1>
                </td>
            </tr>
            <tr>
                <td>
                    <h2>
                        Vous avez reçu une nouvelle prise de Rendez-vous avec les éléments suivants :
                    </h2>

                    <table class="table table-striped" id="orderItems-table">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Prénom</th>
                                <th scope="col">Contact Téléphonique</th>
                                <th scope="col">Lieu de Résidence</th>
                                <th scope="col">Email</th>
                                <th scope="col">Examen Souhaité</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $appointment->last_name }}</td>
                                <td>{{ $appointment->first_name }}</td>
                                <td>{{ $appointment->phone }}</td>
                                <td>{{ $appointment->location_residence }}</td>
                                <td>{{ $appointment->email }}</td>
                                <td>{{ $appointment->getMedicalCheckupsAttribute()->title ?? '---' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
