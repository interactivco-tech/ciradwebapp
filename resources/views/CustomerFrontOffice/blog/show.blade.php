@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    DITES-NOUS DOCTEUR | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>LE BLOG DU DOCTEUR COOL</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - DITES-NOUS DOCTEUR</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="actu-blog">
                <div class="img-actu-blog">
                    <img src="{{ asset('/storage/images/' . $blog->image) }}" alt="" srcset="">
                </div>
                <div class="title-actu-blog">
                    <h2>{{ $blog->title }}</h2>
                </div>
            </div>
            <div class="txt-actu-blog">
                <p>
                    {!! $blog->description !!}
                </p>
            </div>
        </div>
        <div class="sect--">
            <div class="container">
                <div class="sec-img">
                    @if ($blog->file == null)
                        @php
                        $files = explode(',',trim(stripslashes($blog->medias),"[]"));
                        @endphp
                        @foreach ($files as $item)
                            @php
                            $item = trim($item, '"');
                            $url = "/storage/$item";
                            $arr = explode('/', $url);
                            $last = end($arr);
                            $download_link = "/download/$last";
                            @endphp
                            <a href="{{ url($download_link) }}">
                                <img src="{{ asset($url) }}" height="350px" width="1400px"/>
                            </a>
                        @endforeach
                    @else
                        Aucune image de disponible
                    @endif
                </div>
            </div>
        </div>
        <div class="blog">
            <div class="title-page">
                <h2>CONSULTER LES AUTRES ARTICLES</h2>
            </div>
            <div class="container">
                <div class="choice--">
                            @foreach ($blogs as $blog)
                                <div class="item-blog">
                                    <div class="img-blog">
                                        <img src="{{ asset('/storage/images/' . $blog->image) }}" alt="{{ $blog->title }}" alt=""
                                            srcset="">
                                    </div>
                                    <div class="title-blog">
                                        <h4>{{ $blog->title }}</h4>
                                    </div>
                                    <p>
                                        {!! \Illuminate\Support\Str::limit($blog->description, 240, '...') !!}
                                    </p>
                                    <div class="link-blog">
                                        <a href="/detailblog/{{ $blog->slug }}">Lire l'article <i class="fa fa-caret-right"
                                                aria-hidden="true">&nbsp;</i> </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                <!--    <div class="row mt-3">
                            
                        @foreach ($blogs as $blog)
                            <div class="col-sm-4 py-4">
                                <div class="item-bloging">
                                    <div class="img-blog">
                                        <img src="{{ asset('/storage/images/' .$blog->image) }}" alt="{{ $blog->title }}" alt="" srcset="">
                                    </div>
                                    <div class="bloginges">
                                        <div class="title-blog">
                                            <h4>{{ $blog->title }}</h4>
                                        </div>
                                        <p>
                                            {!!\Illuminate\Support\Str::limit($blog->description, 240, '...')!!}
                                        </p>
                                        <div class="link-bloges">
                                            <a href="#">Lire l'article  <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i> </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endforeach -->
                    </div>
                </div>
        </div>
    </div>

@endsection
