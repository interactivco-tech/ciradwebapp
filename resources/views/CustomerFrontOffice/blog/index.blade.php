@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    DITES-NOUS DOCTEUR | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>LE BLOG DU DOCTEUR COOL</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - DITES-NOUS DOCTEUR</h5>
    </div>

    <div class="content" id="blog-1">
        <div class="content-blog">
            @foreach ($blogs as $blog)
                <div class="blo">
                    <div class="img-blog">
                        <img src="{{ asset('/storage/images/'. $blog->image) }}" alt="{{ $blog->title }}" alt=""
                            srcset="">
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="ct-blog">
                                <h3><a href="/detailblog/{{$blog->slug}}">{{ $blog->title }}</a></h3>
                                <p>
                                    {!! \Illuminate\Support\Str::limit($blog->description, 240, '...') !!}
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="btn-read">
                                <a href="/detailblog/{{$blog->slug}}">Lire le contenu <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="pagination">
            <ul class="pagin">
                {!! $blogs->links() !!}
            </ul>
        </div>
    </div>

@endsection
