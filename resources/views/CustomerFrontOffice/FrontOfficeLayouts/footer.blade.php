<div class="content" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="logo-footer">
                    <a href="{{url ('/')}}"><img src="{{asset('customerFront/images/Cirad-footer.png')}}" alt="" srcset=""></a>
                    <p>Centre d'investigations médicales et de radiologie</p>
                </div>
                <div class="RS-footer">
                    <ul>
                        <li><a target="_blank" href="https://web.facebook.com/clinique.cirad"><i class="fa fa-facebook" aria-hidden="true">&nbsp;</i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="content-footer">
                    <ul class="menu-footer">
                        <li><a href="{{url ('/') }}">ACCUEIL</a></li>
                        <li><a href="{{route ('a-propos') }}">LE CIRAD</a></li>
                     <!--   <li><a href="{{route ('blog') }}">DITES NOUS DOCTEUR</a></li> -->
                        <li><a href="{{route ('contacter-nous') }}">CONTACTS</a></li>
                    </ul>

                    <div class="newletters">
                        <div class="title-news">
                            <h4>ABONNEZ-VOUS À LA NEWSLETTER</h4>
                        </div>
                        <form action="{{ url('footer') }}" method="POST" id="form500">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <input type="text" required class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nom & prénoms*" name="name" value="{{old('name')}}">
                                </div>
                                <div class="col">
                                    <input type="email"  required class="form-control @error('email') is-invalid @enderror" id="email" placeholder="E-mail*" name="email" value="{{old('email')}}">
                                </div>
                            </div>
                            <div class="link-s">
                                <button type="submit" onclick="" value="Envoyer">Soumettre <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content" id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="copy">
                    <ul>
                       <!-- <li>Mentions Légales</li> -->
                      <!--  <li>Politique de Confidentialité</li> -->
                        <li>Plan du Site</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="right">
                    <p>Copyright CIRAD 2021 - Réalisé avec le Coeur par <a target="_blank" href="https://vanessalecosson.net/prestations-digitales/">INTERACTIVCO</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
