<div id="header">
    <div class="infoS-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="RS-head">
                        <ul>
                            <li><a target="_blank" href="https://web.facebook.com/clinique.cirad"><i class="fa fa-facebook" aria-hidden="true">&nbsp;</i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="urgence">
                    <a href="tel:+22527 22 528 528"><i class="fa fa-phone" aria-hidden="true">&nbsp;</i></a>
                        <ul>
                            <li class="img-h"><img src="{{asset('customerFront/images/casque.png')}}" alt="" srcset=""></li>
                            <li class="txt-urg">Centre d’appel +225 27 22 528 528 </li>
                        </ul>
                    </div>
                </div>
           <!--     <div class="col-sm-4">
                    <div class="search">
                        <div class="search-box">
                            <input class="search-txt" type="text" name="" placeholder="Recherchez ici ">
                            <a class="search-btn" href="#">
                                <i class="fa fa-search" aria-hidden="true">&nbsp;</i>
                            </a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>

    <div class="content-header">
        <div class="logo">
            <a href="{{ url('/') }}"><img src="{{asset('customerFront/images/Logo CIRAD.png')}}" alt="" srcset="" class="main-logo"></a>
            <a href="{{ url('/') }}"><img src="{{asset('customerFront/images/logo-blanc.png')}}" alt="" srcset="" class="logo-responsive"></a>
        </div>

    </div>
    <nav class="navbar navbar-expand-sm navbar-dark menu--">
        <!--Toggle Collapse Button--> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <!--Division for navbar-->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!--UL for links-->
            <ul class="navbar-nav mr-auto">

                <li class="nav-item "> <a class="nav-link " href="{{url('/') }}" > ACCUEIL</a></li>
                <li class="nav-item "> <a class="nav-link " href="{{route ('a-propos') }}" > LE CIRAD</a></li>

                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle catogary" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> NOS SPÉCIALITÉS </a>
                    <!--Div for catogary 2-->
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="container">
                            <div class="row item-menu-d">
                                <!--Row 1-->
                                <div class="col-md-4">
                                    <!--Column 1-->
                                    <div class="title-section spe">
                                        <h1>
                                            NOS SPÉCIALITÉS <br/>
                                            ET DOMAINES <br/>
                                            D'EXPERTISE
                                        </h1>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <!--Column 2-->
                                    <div class="link-cirad-1">
                                        <div class="link-content">
                                            <ul class="sp--">

                                                <li><a href="{{route ('irm') }}">IRM</a></li>
                                                <li><a href="{{route ('scanner') }}">Scanner</a></li>
                                                <li><a href="{{route ('mammographie') }}">Mammographie</a></li>
                                                <li><a href="{{route ('echographie') }}">Echographie</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <!--Column 3-->
                                    <div class="link-content">
                                        <ul class="sp--">

                                            <li><a href="{{route ('radiographie') }}">Radiographie</a></li>
                                            <li><a href="{{route ('analyse-mediacale') }}">Biologie médicale</a></li>
                                            <li><a href="{{route ('bilan-sante') }}">Bilan de santé</a></li>
                                            <li><a href="{{route ('biopsie') }}">Biopsie</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </li>
         <!--       <li class="nav-item "> <a class="nav-link " href="{{route ('blog') }}" >  DITES NOUS DOCTEUR  </a></li>  Catagory 4-->
                <li class="nav-item "> <a class="nav-link " href="{{route ('contacter-nous') }}" > CONTACTS</a></li>
          <!--      <li class="nav-item "> <a class="nav-link " href="{{route ('prise-de-rendez-vous') }}" > PROGRAMMER UN RENDEZ-VOUS</a></li> -->
            </ul>
        </div>
    </nav>

</div>


