<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" >

        <!-- Link boostrap -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">

        <!-- Link slick -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">

        <!--FontAwesome librairies-->
        <link rel="stylesheet" href="{{asset('customerFront/font-awesome-4.7.0/css/font-awesome.min.css')}}">

        <!--Owl caroussel-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">



        <!--Link to add css -->
        <link rel="stylesheet" href="{{asset('customerFront/css/search.css')}}">
        <link rel="stylesheet" href="{{asset('customerFront/css/index.css')}}">
        <link rel="stylesheet" href="{{asset('customerFront/css/menu.css')}}">


    </head>

    <body>

            @include('CustomerFrontOffice.FrontOfficeLayouts.header')

        <div id="content-page">

            @yield('content')

            @include('CustomerFrontOffice.FrontOfficeLayouts.footer')

        </div>

        <!--Jquery librairies -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!--Bootstrap javascript-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

        <!--slick -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

        <!--Owl caroussel-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

        <script src="{{asset('customerFront/js/Owlcaroussel.js')}}"></script>

        <!--Other Js-->
        <script src="{{asset('customerAssets/js/jquery.jold.paginator.min.js')}}"></script>  
        <script src="{{asset('customerFront/js/menu.js')}}"></script>
        <script src="{{asset('customerFront/js/menu-scroll.js')}}"></script>
        <script src="{{asset('customerFront/js/paggination.js')}}"></script>
        <script src="{{asset('customerFront/js/slick.js')}}"></script>

        <!--AOS-->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!-- AOS animation -->
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    </body>

    <script src="text/javascript">
       AOS.init();
    </script>

</html>
