@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Nous Contacter | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOUS CONTACTER</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - CONTACTS</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>Vous avez une urgence, une préoccupation, contactez-nous !</h4>
            </div>
            <p>
                Rentrez en contact avec nos services
                pour une prise en charge rapide.
            </p>
          <!--  <div class="row">
                <div class="col-sm-6">
                    <div class="urgence--">
                        <p>
                            Rentrez en contact avec nos services <br />
                            pour une prise en charge rapide.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-urgence">
                        <p>
                            Vous cherchez des réponses
                            à vos interrogations ?<br />
                            <span class="bb">Consultez le blog du CIRAD</span>
                        </p>
                        <div class="btn-block">
                            <a href="#">Ouvrir le blog</a>
                        </div>

                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <div class="content" id="radio">
        <div class="sect-radio">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio--">
                            <h3>Radiographie et Imagerie</h3>
                            <ul>
                                <li><span class="r-sp">Tel :</span> +225 27 22 52 85 28 / +225 27 22 50 95 09 </li>
                                <li><span class="r-sp">Mobile :</span> + 225 07 59 09 95 00 / +225 07 08 36 24 24</li>
                                <li><span class="r-sp">E-mail :</span> imagerie@cirad.ci</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="txt-radio">
                            <h3>Laboratoire d'analyse</h3>
                            <ul>
                                <li><span class="txt-sp">Tel :</span> +225 27 22 52 95 29 </li>
                                <li><span class="txt-sp">Mobile :</span> + 225 07 77 53 11 53</li>
                                <li><span class="txt-sp">E-mail :</span> Bio@cirad.ci</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>
    <div class="content" id="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31778.860195879417!2d-3.988576875255595!3d5.362306079118315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1eb55855ab757%3A0xcbf6db3a54291a25!2sCIRAD!5e0!3m2!1sen!2sci!4v1623632921871!5m2!1sen!2sci"
            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>

@endsection
