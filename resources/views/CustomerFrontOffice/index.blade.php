@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Accueil | {{ config('app.name') }}
@endsection

@section('content')

    <div class="slider-home">
        <!-- Set up your HTML -->
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach ($slides as $slide)
                    <div class="carousel-item active">
                        <img src="{{ asset('/storage/images/' . $slide->image) }}" alt="{{ $slide->title }}" srcset="">
                        <div class="txt-slide">
                            <h2>
                                {{ $slide->title }}<br/> {{ $slide->subtitle }}
                            </h2>
                            <div class="link-cirad">
                                <a href="{{ route('a-propos') }}" class="link">Découvrez l'univers du CIRAD <i
                                        class="fa fa-caret-right" aria-hidden="true">&nbsp;</i> </a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('/storage/images/' . $slide->image) }}" alt="{{ $slide->title }}" srcset="">
                        <div class="txt-slide">
                            <h2>
                                {{ $slide->title }}<br /> {{ $slide->subtitle }}
                            </h2>
                            <div class="link-cirad">
                                <a href="{{ route('a-propos') }}" class="link">Découvrez l'univers du CIRAD <i
                                        class="fa fa-caret-right" aria-hidden="true">&nbsp;</i> </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="content" id="why-choose-cirad">
        <div class="title-content">
            <h2>LES ATOUTS DU CIRAD </h2>
        </div>
        <p class="dt-choice">
            Toutes les explorations médicales disponibles avec
        </p>

        <div class="container choice">
            <div class="choice--">
                <div class="bloc-choice">
                    <div class="item-choice">
                        <div class="img-item-choice img-1">
                            <img src="{{ asset('customerFront/images/i-teams.png') }}" alt="" srcset="">
                        </div>
                        <p>
                            Une équipe médicale hautement qualifiée qui s'engage à vous faire vivre la meilleure expérience
                        </p>
                    </div>
                </div>
                <div class="bloc-choice">
                    <div class="item-choice">
                        <div class="img-item-choice img-2">
                            <img src="{{ asset('customerFront/images/i-cirad.png') }}" alt="" srcset="">
                        </div>
                        <p>
                            Des équipements de dernière génération pour vous garantir un diagnostic précis et fiable
                        </p>
                    </div>
                </div>
                <div class="bloc-choice">
                    <div class="item-choice">
                        <div class="img-item-choice">
                            <img src="{{ asset('customerFront/images/i-environnement.png') }}" alt="" srcset="">
                        </div>
                        <p>
                            Un environnement médical conçu pour votre détente et votre bien-être
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="what-we-do">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="slide-cirad">
                        <div class="title-we-do">
                            <h2>CE QUE NOUS OFFRONS </h2>
                        </div>

                        <p class="dt-choice">
                            Un panel d’examens respectant les normes nationales et internationales
                        </p>

                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="cirad">
                                        <h4>CIRAD</h4>
                                        <p> Toute l’imagerie médicale de dernière génération à Abidjan
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="img-cirad">
                                        <img src="{{ asset('customerFront/images/c1.png') }}" alt="" srcset="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="img-cirad">
                                        <img src="{{ asset('customerFront/images/c2.png') }}" alt="" srcset="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="carousel-item">
                    <div class="slide-cirad-bio">
                        <div class="title-we-do">
                            <h2>CE QUE NOUS OFFRONS </h2>
                        </div>

                        <p class="dt-choice">
                            Un laboratoire médical équipé de matériels de haute précision au service de votre santé
                        </p>

                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="img-cirad">
                                        <img src="{{ asset('customerFront/images/cirad-bio1.png') }}" alt="" srcset="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="img-cirad">
                                        <img src="{{ asset('customerFront/images/cirad-bio2.png') }}" alt="" srcset="">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="cirad-bio">
                                        <h4>CIRAD BIO</h4>
                                        <p>
                                            Votre laboratoire médical au coeur de l’innovation et au service de votre santé
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="content" id="testimony">
        <div class="container">
            <div class="header-blog">
                <div class="title-content">
                    <h2>TÉMOIGNAGES</h2>
                </div>
                <p class="dt-choice">
                    Ce que nos patients disent de nous...
                </p>
            </div>
            <div class="owl-carousel owl-theme owl-carousel">
                @foreach ($testimonials as $testimonial)

                    <div class="item item-testimony">
                        <div class="title-testimony">
                            <h3>{{ $testimonial->title }} </h3>
                        </div>
                        <div class="content-testimony">
                            <p>
                                {!! $testimonial->description !!}
                            </p>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
        <div class="cirad-en-video">
            @foreach ($videoStores as $videoStore)
                <div class="header-blog">
                    <div class="title-content">
                        <h2>{{ $videoStore->title }} </h2>
                    </div>
                    <p class="dt-choice">
                        {{ $videoStore->subtitle }}
                    </p>
                </div>
                <div class="container img-v">
                    <iframe class="embed-responsive-item" width="560" height="315" src="{{ $videoStore->link_youtube }}"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            @endforeach
        </div>
    </div>

    </div>
 <!--   <div class="content" id="blog">
        <div class="container">
            <div class="header-blog">
                <div class="title-blog">
                    <h2>CONSULTER LE BLOG </h2>
                </div>

                <p class="dt-choice">
                    Des conseils et astuces de nos médecins
                </p>
            </div>

            <div class="choice--">
                @foreach ($blogs as $blog)
                    <div class="item-blog">
                        <div class="img-blog">
                            <img src="{{ asset('/storage/images/' . $blog->image) }}" alt="{{ $blog->title }}" alt=""
                                srcset="">
                        </div>
                        <div class="title-blog">
                            <h4>{{ $blog->title }}</h4>
                        </div>
                        <p>
                            {!! \Illuminate\Support\Str::limit($blog->description, 240, '...') !!}
                        </p>
                        <div class="link-blog">
                            <a href="/detailblog/{{ $blog->slug }}">Lire l'article <i class="fa fa-caret-right"
                                    aria-hidden="true">&nbsp;</i> </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div> -->

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{ route('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i
                        class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
