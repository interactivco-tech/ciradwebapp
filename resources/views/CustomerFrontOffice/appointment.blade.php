@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Prise de Rendez-vous | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>PRISE DE RENDEZ-VOUS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - programmer un rendez-vous</h5>
    </div>

    <div class="content" id="form-rdv">
        <div class="container">
            <div class="title-bloc">
                <h4>Formulaire de prise de rendez-vous</h4>
            </div>
            <div class="formul">
                <h4>PRÉSENTATION DU PATIENT </h4>
                <form action="{{ url('appointmentess') }}" method="POST" id="form600">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6 block1">
                            <div class="label--">
                                <label for="iSexe">Civilité</label>
                            </div>
                            <div class="lign-">
                                <select id="iSexe" required class="form-control @error('sexe') is-invalid @enderror" name="sexe" value="{{old('sexe')}}">
                                    <option selected>Choisir une option </option>
                                    <option>Masculin</option>
                                    <option>Feminin</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group col-md-6 block-dt">
                        <div class="label-dt">
                            <label for="iDate">Date de naissance</label>
                        </div>
                        <div class="lign-dt">
                            <input type="date" required class="form-control @error('birthdate') is-invalid @enderror" id="iDate" name="birthdate"  >
                        </div>

                    </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 block1">
                            <div class="label--">
                                <label for="iN">Nom*</label>
                            </div>
                            <div class="lign-">
                                <input type="text" required class="form-control @error('last_name') is-invalid @enderror" id="iN" name="last_name" value="{{old('last_name')}}">
                            </div>
                        </div>
                        <div class="form-group col-md-6 block1">
                            <div class="label--">
                                <label for="iPre">Prénom(s)*</label>
                            </div>
                            <div class="lign-">
                                <input type="text" required class="form-control @error('first_name') is-invalid @enderror" id="iE" name="first_name" value="{{old('first_name')}}">
                            </div>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <div class="label--">
                                <label for="iE">E-mail*</label>
                            </div>
                            <div class="lign-">
                                <input type="email" required class="form-control @error('email') is-invalid @enderror" id="iPre" name="email" value="{{old('email')}}">
                            </div>

                        </div>
                        <div class="form-group col-md-6">
                            <div class="label--">
                                <label for="iTe">Téléphone*</label>
                            </div>
                            <div class="lign-">
                                <input type="text" required class="form-control @error('phone') is-invalid @enderror" id="iTe" name="phone" value="{{old('phone')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">

                            <div class="label--">
                                <label for="iCR">Commune de résidence*</label>
                            </div>
                            <div class="lign- c-input">
                                <input type="text" required class="form-control @error('location_residence') is-invalid @enderror" id="iCR" name="location_residence" value="{{old('location_residence')}}">
                            </div>

                        </div>
                    </div>

                    <div class="form-row fff-">
                        <div class="form-group col-md-12">
                            <div class="lb-cirad-choice">
                                <label for="iSexe">Avez-vous déjà été reçu au CIRAD pour des examens? </label>
                            </div>
                            <div class="cirad-choice">
                                <select required id="iSexe" class="form-control @error('received_by_cirad') is-invalid @enderror" name="received_by_cirad" value="{{old('received_by_cirad')}}">
                                    <option selected>Choisir une option </option>
                                    <option>Oui</option>
                                    <option>Non</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <h4>RENSEIGNEMENTS RELATIFS À L'EXAMEN SOUHAITÉ</h4>

                    <div class="form-row exam">
                        <div class="form-group col-md-12">
                            <div class="sec-1">
                                <select id="medical_Checkups_id" class="form-control @error('medical_Checkups_id') is-invalid @enderror" name="medical_Checkups_id">
                                    <option selected>Choisir l'examen </option>
                                    @foreach ($medicalCheckups as $medicalCheckup)
                                        <option value="{{$medicalCheckup->id}}">{{$medicalCheckup->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                         <!--   <div class="sec-2">
                                <input type="date" required placeholder="date" class="form-control @error('appointment_date') is-invalid @enderror" name="appointment_date" value="{{old('appointment_date')}}">
                            </div>
                            <div class="sec-3">
                                <input type="time" required class="form-control @error('appointment_time') is-invalid @enderror" placeholder="Heure" name="appointment_time" value="{{old('appointment_time')}}">
                            </div> -->

                        </div>
                    </div>

                    <div class="case-a-cocher">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Les informations que j'ai fournies sont exactes et complètes
                            </label>
                        </div>
                    <!--    <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Je veux recevoir un SMS ou un appel de rappel 24 h avant mon RDV
                            </label>
                        </div> -->
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                J'accepte les termes et conditions
                            </label>
                        </div>
                    </div>
                    <div class="link-soumission">
                        <button type="submit" onclick="" class="btn-submit" value="Envoyer">Soumettre</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection



