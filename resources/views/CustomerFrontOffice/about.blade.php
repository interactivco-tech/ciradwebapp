@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    A propos de Nous | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>L’UNIVERS DU CIRAD</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - LE CIRAD</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>Notre histoire</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history image-history">
                        <img src="{{asset('customerFront/images/Ciradcc.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        Le CIRAD est un centre d’investigations médicales, situé aux II Plateaux 7ème tranche Immeuble TERA qui
                        s’est donné pour mission d’offrir les meilleurs services d’exploration à la population.
                        Inauguré en 2012,  il s’est fixé pour objectif de faire  passer l’offre de soins de santé en Côte d’Ivoire
                        au niveau supérieur en s’équipant d’appareils à la pointe de la technologie afin d’établir les diagnostics
                        (laboratoire et imagerie) les plus précis. Il dispose également d’une ambulance.<br/><br/>
                        Au-delà de l'équipement, de la technologie, de la conception et de la structure de ses installations, la principale force du CIRAD reste son équipe : efficace et expérimentée travaillant activement pour offrir aux patients un accueil de qualité et une prise en charge personnalisée. 

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="nos-valeurs">
        <div class="container">
            <div class="title-bloc">
                <h4>Nos Valeurs</h4>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="img-valeur">
                            <img src="{{asset('customerFront/images/valeur-icone-bienveillance.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="txt-valeur">
                            <h4>COURTOISIE</h4>
                            Garantir un service à la hauteur des attentes de la patientèle, c’est là notre engagement : 
                            notre priorité reste la satisfaction de tous ceux qui sollicitent nos services.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="txt-valeur">
                            <h4>EMPATHIE</h4>
                            C’est avec la pleine conscience de l’état médical de nos patients que nous veillons chaque 
                            jour à assurer le meilleur diagnostic avec le meilleur service en veillant au bien être moral de chacun.
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-valeur1">
                            <img src="{{asset('customerFront/images/core-value-line-icon-on-white-vector.png')}}" alt="" srcset="">
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="img-valeur">
                            <img src="{{asset('customerFront/images/1528669.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="txt-valeur">
                            <h4>PROFESSIONNALISME</h4>
                            Le CIRAD, c’est avant tout un personnel hautement qualifié mais aussi
                            des équipements de haute technologie visant à fournir des résultats précis et fiables.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!--  <div class="content" id="missions">
        <div class="container">
            <div class="title-bloc">
                <h4>Notre mission</h4>
            </div>
            <div class="img-mission">
                <img src="{{asset('customerFront/images/mission.png')}}" alt="" srcset="">
            </div>

            <div class="content-mission">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
                    orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>
        </div>
    </div> -->
    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                <div class="hour-cirad">
                    <h4>Horaires CIRAD</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="days">
                                <ul>
                                    <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                    <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="hours-h">
                                <ul>
                                    <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="hour-cirad-bio">
                    <h4>Horaires CIRAD BIO</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="days">
                                <ul>
                                    <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                    <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="hours-h">
                                <ul>
                                    <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous</a>
            </div>
        </div>
    </div>

@endsection
