@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Biopsie | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - BIOPSIE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>BIOPSIE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/Biopsie1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        Une biopsie consiste à prélever un fragment de tissu ou d’organe, au moyen d’une aiguille.<br/> L’objectif est de réaliser un examen microscopique et/ou une analyse biochimique pour déceler diverses anomalies. 
                        La biopsie permet aussi d’évaluer la progression d’une maladie si celle-ci est connue et de décider d’un traitement adapté.<br/>
                        Le prélèvement de tissu ou de cellules peut concerner presque n’importe quelle partie du corps (sein, foie, rein, poumon, utérus, prostate, testicule, etc.)

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/Biopsie2.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                    Le médecin réalise une anesthésie locale si cela est nécessaire.<br/> Pendant l’intervention, 
                    il peut avoir recours à des techniques d’imagerie (comme l’échographie) pour repérer de manière très précise la zone où sera pratiqué le prélèvement.
                    La durée de l’intervention est de 10 à 30 minutes. 
                </p>
            </div>
        </div>
    </div>
    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
