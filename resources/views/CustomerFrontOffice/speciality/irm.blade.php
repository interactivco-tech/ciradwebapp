@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    IRM | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - IRM</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>IRM</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/IRM-1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        L'imagerie par résonance magnétique nucléaire est un examen qui utilise un champ magnétique (aimant puissant) et des ondes radios. 
                        Il permet de réaliser des images ultra précises de l’intérieur du corps humain en 2 ou 3 dimensions.<br/>

                            L’IRM est prescrite par les médecins pour explorer en profondeur une pathologie donnée
                            afin de fournir au patient un traitement efficace et optimal. Elle concerne toutes les spécialités :<br/>
                            •	Neurologie<br/>
                            •	Traumatologie<br/>
                            •	Rhumatologie<br/>
                            •	Gynécologie.<br/>
                            •	Gastro-entérologie<br/>
                            •	Cancérologie <br/>

                            C’est un examen absolument non irradiant et sans douleur.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            <div class="title-bloc">
                <h4>Précautions avant l'examen</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/IRM.jpg')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                L’équipe médicale demande au patient de se dévêtir, d’enfiler une blouse, de retirer tout bijou ou accessoire métallique qui pourrait interférer avec le champ magnétique dans la salle d'examen.<br/><br/>
                L’IRM est contre indiquée chez les personnes porteuses de matériel métallique (implants cochléaires, clips chirurgicaux, stimulateurs cardiaques).  <br/><br/>


                </p>
            </div>
        </div>
    </div>

    <div class="content" id="during-exam">
        <div class="container">
            <div class="title-bloc">
                <h4>Durant l'examen... </h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/IRM-3.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                Durant toute la durée de l’examen, l’équipe médicale reste en contact visuel et auditif avec le patient qui doit rester absolument immobile (certains mouvements peuvent détériorer la qualité des images).<br/><br/> 
                En cas de malaise, celui-ci peut communiquer avec le technicien qui interviendra aussitôt.<br/><br/>


                </p>
            </div>
            <div class="a-savoir">
                <div class="title-bloc">
                    <h4>CE QUI PEUT ÊTRE DIFFICILE</h4>
                </div>

                <div class="content-a-savoir">
                    <strong>Le froid </strong> : la salle est climatisée pour le bon fonctionnement de la machine.<br/>
                    <strong >Le bruit </strong> : il est répétitif à l’intérieur du tunnel. Pour l’atténuer vous avez le choix entre des bouchons et un casque.<br/>
                    <strong >Le tunnel </strong> : il est étroit et peut donc être angoissant. Mais le personnel médical est proche de vous, vous voit, vous entend et vous parle pour vous rassurer.<br/>
                    <strong> L’injection éventuelle du produit de contraste </strong> : elle n’est pas douloureuse mais peut parfois générer une sensation de chaleur surprenante et passagère.<br/>

                </div>
            
            </div>
        </div>
        
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
