@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Analyse Médiacle | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - ANALYSE MEDICALE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>ANALYSE MEDICALE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/biologie-medicale2.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                            La biologie médicale désigne une spécialité qui a recours à des techniques de laboratoire (analyse, microscopie, immunologie, bactériologie, virologie, hématologie, biochimie etc.)<br/>
                            pour contribuer notamment à l’évaluation de l’état de santé, au diagnostic de pathologies, au suivi de traitements...
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/biologie-medicale1.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                    La biologie médicale recouvre plusieurs branches :
                </p>
                <ul>
                    <li>Hématologie biologique (diagnostic des pathologies du sang),</li>
                    <li>Biochimie clinique</li>
                    <li>Microbiologie (bactérie, virus, parasite, champignons)</li>
                    <li>Biologie de la reproduction (analyse des liquides séminaux et <br/>aide technique à la procréation médicalement assistée),</li>
                    <li>Immunologie (étude des mécanismes de défense de l’organisme ;<br/> essentielle lors des greffes et transplantations),</li>
                    <li>Anatomie pathologique (frottis cervical, biopsies, cytologie, etc.)</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
