@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
   Scanner | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - SCANNER</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>SCANNER</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/scanner1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        Le Scanner ou tomodensimétrie réalise de manière ciblée des clichés en coupes fines du corps, en combinant l’utilisation des rayons X à un système informatisé. Les images ainsi obtenues sont reconstituées en trois dimensions ; ce qui a pour avantage de donner des informations très précises sur les zones étudiées (cerveau, abdomen, cage thoracique, vaisseaux, …) 
                        Il permet la recherche des anomalies non détectables à la radio ou à l’échographie.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            <div class="title-bloc">
                <h4>Précautions avant l'examen</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/scanner2.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                L’examen peut nécessiter d’être à jeun. Dans ce cas,
                le patient doit arrêter de prendre tout aliment solide au moins 5h avant son rendez-vous. 
                </p>
                <h3>SIGNALER SYSTEMATIQUEMENT</h3>
                <div class="bloc-info-examen">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Toute allergie à l’iode.</li>
                                    <li>
                                    Tout traitement médical en cours.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Toute grossesse.
                                    </li>
                                    <li>
                                    Toute affection chronique (insuffisance rénale, asthme, cardiaque)
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="during-exam">
        <div class="container">
            <div class="title-bloc">
                <h4>Durant l'examen... </h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/scanner3.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                Le patient est allongé sur une table mobile qui se déplace dans un anneau.
                Il reste en contact continu avec le technicien qui s’assure que tout se déroule bien.
                La durée de l’examen est en moyenne de 10 à 30 minutes. 
                </p>

            </div>
        </div>
    </div>

    <div class="content" id="after-exam">
        <div class="container">
            <div class="title-bloc">
                <h4>APRES L’EXAMEN </h4>
            </div>
            <div class="content-examen">
                <p>
                Si le patient a bénéficié d’une injection d’iode, il doit s’hydrater en absorbant dans les heures suivant l’examen environ 1,5 litres d’eau.
                </p>

            </div>
        </div>
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
