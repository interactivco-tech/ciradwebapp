@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Bilan de Santé | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - BILAN DE SANTE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>BILAN DE SANTE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/Bilan-santé.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        Le bilan de santé est une action préventive qui a pour but de déceler d’éventuelles pathologies afin d'en proposer une prise en charge précoce.<br/>
                        Le patient bénéficie d’une série d’examens qui permettent d’explorer son état de santé.<br/> Cette séance dure en moyenne 2 heures et comporte plusieurs analyses et tests élaborés en fonction de l’âge, du sexe et des antécédents médicaux.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            
            <div class="title-bloc">
                <h4>LE BILAN DE SANTE AU CIRAD – LES FORMULES</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/Bilan-santé2.png')}}" alt="" srcset="">
            </div>
            <div class="content-examen">
                <div class="title">
                    <h3>BILAN IMAGERIE MEDICALE</h3>
                </div>
                <div class="row">
                 <div class="col-sm-6">
                     <div class="sec">
                         <h3>LE BILAN STANDARD - [120.000 F CFA]</h3>
                         <ul>
                             <li>Radiographie Pulmonaire</li>
                             <li>Echographie abdominale</li>
                             <li>Echographie Pelvienne</li>
                             <li>Echographie Thyroïdienne</li>
                             <li>Echodoppler des vaisseaux du cou</li>
                         </ul>
                     </div>
                 </div>
                 <div class="col-sm-6">
                    <div class="sec">
                            <h3>LE BILAN PREMIUM - [175.000 F CFA]</h3>
                            <ul>
                                <li>Radiographie Pulmonaire</li>
                                <li>Echographie abdominale</li>
                                <li>Echographie Pelvienne</li>
                                <li>Echographie Thyroïdienne</li>
                                <li>Echodoppler des vaisseaux du cou</li>
                                <li>E.C.G</li>
                                <li>Echodoppler du cœur</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="title">
                    <h3>BILAN BIOLOGIQUE</h3>
                </div>
                
                <p>Son objectif est d’évaluer les risques suivants : Cardio-vasculaire, diabétique, rénal, hépatique, cancéreux et hématologique</p>
                
                <div class="row">
                 <div class="col-sm-6">
                     <div class="sec">
                         <h3>LE BILAN STANDARD POUR HOMME ET FEMME [175.000 F CFA]</h3>
                         <ul>
                             <li>Créatine</li>
                             <li>Glycémie</li>
                             <li>Cholestérol total</li>
                             <li>Triglycérides</li>
                             <li>HDL</li>
                             <li>LDL</li>
                             <li>NFS</li>
                             <li>Calcium</li>
                             <li>Magnésium</li>
                             <li>Acide unique</li>
                             <li>Transaminases</li>
                             <li>Antigène HBS</li>
                             <li>AC anti HBC taux</li>
                             <li>AC anti HBC IgM</li>
                             <li>VHC ( Hépatite C )</li>
                             <li>Hémoglobine glyquée</li>
                         </ul>
                     </div>
                 </div>
                    <div class="col-sm-6">
                        <div class="sec">
                            <h3>LE BILAN STANDARD POUR HOMME + 40 ANS [200.000 F CFA]</h3>
                            <p>Bilan standard + PSA total (exploration du risque de cancer de la prostate)</p>
                        </div>
                    </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
