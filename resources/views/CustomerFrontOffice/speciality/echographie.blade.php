@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Echographie | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - ECHOGRAPHIE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>ECHOGRAPHIE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/echo1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        L'échographie est une technique d’imagerie médicale qui utilise les ultrasons. Elle permet d’analyser la plupart des organes (foie, pancréas, utérus, prostate, thyroïde, etc.) <br/>
                        C’est un examen qui permet également de réaliser des biopsies au cours desquelles le médecin prélève un échantillon qui peut être analysé au laboratoire. <br/><br/>

                        <b>LES SPECIFICITES AU CIRAD</b>

                        Les échographies sont réalisées avec différents types de sondes en fonction de la zone du corps à étudier (superficielle, profonde, endocavitaire, doppler…)


                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            <div class="title-bloc">
                <h4>UNE ECHOGRAPHIE AU CIRAD</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/echo2.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                Notre centre est équipé d'un appareil de haute performance qui permet de réaliser tous les examens possibles en échographie.
                </p>

                <div class="bloc-info-examen">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Abdomen</li>
                                    <li>
                                    Pelvis (sus pubien et endocavitaire)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Obstétrique</li>
                                    <li>Seins</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Thyroïde</li>
                                    <li>
                                    Prostate (sus pubien et endocavitaire)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Testiculaire</li>
                                    <li>Ostéoarticulaire</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="during-exam">
        <div class="container">
            <div class="title-bloc">
                <h4>PRECAUTIONS AVANT L’EXAMEN </h4>
            </div>
            
            <div class="content-examen">
                
                <div class="bloc-info-examen">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="cas-">
                                <h4>Echographie de l’abdomen</h4>
                                •	Être à jeun au moins cinq heures avant l’examen.<br/>
                                •	Prendre des médicaments uniquement quand cela ne nécessite pas une prise alimentaire.<br/>
                                •	Prévoir une collation pour les diabétiques aussitôt après l’examen.<br/>
                                •	Prévoir une collation pour les enfants.
                            </div>
                            <div class="cas-">
                                <h4>Vessie et reins</h4> 
                                •	Être à jeun depuis 5 heures.<br/>
                                •	Boire 1,5 litre d’eau 1 heure avant l’examen.<br/>

                            </div>
                            <div class="cas-">
                                <h4>Pelvis</h4> 
                                •	Il n’y a pas de contrainte de jeûne.<br/>
                                •	Si l’examen se fait par voie sus pubienne, boire 1,5 litre d’eau avant.<br/>
                                •	En cas d’examen par voie endocavitaire, il n’est pas nécessaire de boire.

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="txt-">
                                <h3>Aucune précaution particulière n'est prévue pour les autres examens (thyroïde, testicules, seins ...) </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
