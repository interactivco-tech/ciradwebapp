@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Mammographie | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - MAMMOGRAPHIE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>MAMMOGRAPHIE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/mamo1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        La mammographie est une imagerie du sein qui utilise des rayons X à faible dose. C’est un examen qui joue un rôle capital dans la détection précoce des cancers.  Il peut montrer d’éventuelles anomalies (nodules, distorsions architecturales, microcalcifications, …)  avant que la patiente ne commence à ressentir les premiers symptômes. 
                        L’examen dure environ trente minutes et est systématiquement couplé à une échographie complémentaire.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            <div class="title-bloc">
                <h4>RECOMMANDATION AVANT L’EXAMEN</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/mamo2.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                Le médecin a besoin de connaître tous les antécédents médicaux et chirurgicaux
                de la patiente afin d’analyser au mieux la mammographie qui sera réalisée.  
                </p>

                <h3>IL EST ÉGALEMENT RECOMMANDÉ DE</h3>
                <div class="bloc-info-examen">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                   <!-- <li>Choisir son rendez-vous entre 4ème et le 8ème jour suivant la fin des règles.</li> -->
                                    <li>
                                    Ne pas appliquer de déodorant, de poudre (talc) le jour de l’examen</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="i-ss">
                                <ul>
                                    <li>Mettre à disposition du médecin les résultats d’examens mammaires antérieurs
                                    </li>
                                    <li>
                                    S’habiller de sorte à retirer facilement le haut du vêtement avant l’examen.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="during-exam">
        <div class="container">
            <div class="title-bloc">
                <h4>Durant l'examen... </h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/mamo2.png')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                Pendant la mammographie, le sein est placé sur l’appareil et comprimé pendant moins d’une minute.
                Cette compression peut être désagréable, mais est obligatoire car une compression insuffisante peut empêcher de voir une anomalie.<br/>
                Deux ou trois clichés du sein sont ensuite effectués. 


                </p>

                <div class="bloc-info-examen">
                    <h3>CONTRE-INDICATION </h3>
                    <p>Si la patiente est enceinte ou croit l’être, un report de la mammographie est indispensable. Sauf cas exceptionnel.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
