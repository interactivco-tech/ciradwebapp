@extends('CustomerFrontOffice.FrontOfficeLayouts.master')

@section('title')
    Radiographie | {{ config('app.name') }}
@endsection

@section('content')

    <div class="title-page">
        <h2>NOS SPÉCIALITÉS</h2>
    </div>
    <div class="title-brea">
        <h5>ACCUEIL - NOS SPÉCIALITÉS - RADIOGRAPHIE</h5>
    </div>

    <div class="content" id="univers">
        <div class="container">
            <div class="title-bloc">
                <h4>RADIOGRAPHIE</h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-history">
                        <img src="{{asset('customerFront/images/radiologie1.png')}}" alt="" srcset="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="txt-history">
                        <p>
                        La radiographie classique est un examen qui utilise les rayons X.<br/> 
                        La zone du corps à examiner est positionnée devant un appareil qui transforme les rayons en images. <br/>
                        Tous les organes de la tête aux pieds peuvent être radiographiés (squelette, poumons, abdomen). <br/>
                        Elle permet de détecter un grand nombre de lésions :
                        </p>
                        <ul>
                            <li>Fracture</li>
                            <li>Arthrose</li>
                            <li>Malformations</li>
                            <li>Tuberculose</li>
                            <li>Tumeurs</li>
                            <li>Calculs</li>
                            <li>Occlusions</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" id="before-examen">
        <div class="container">
            <div class="title-bloc">
                <h4>CONTRE-INDICATION ABSOLUE</h4>
            </div>
            <div class="img-before-examen">
                <img src="{{asset('customerFront/images/radio.jpg')}}" alt="" srcset="">
            </div>

            <div class="content-examen">
                <p>
                 En cas de grossesse, tout examen radiographique est proscrit sauf cas d’urgence vitale. 
                 En cas de doute, il est préférable de reporter l’examen. 
                </p>
            </div>
        </div>
    </div>

    <div class="content" id="hours">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hour-cirad">
                        <h4>Horaires CIRAD</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 18H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 8H à 13H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hour-cirad-bio">
                        <h4>Horaires CIRAD BIO</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="days">
                                    <ul>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Lundi au vendredi</li>
                                        <li><i class="fa fa-calendar" aria-hidden="true">&nbsp;</i> Samedi</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="hours-h">
                                    <ul>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 17H</li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true">&nbsp;</i> 7H à 12H</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rdv">
                <a href="{{route ('prise-de-rendez-vous') }}">Programmez votre prochain rendez-vous chez nous <i class="fa fa-caret-right" aria-hidden="true">&nbsp;</i></a>
            </div>
        </div>
    </div>

@endsection
