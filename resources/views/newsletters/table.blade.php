<div class="card-body">

    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($newsletters as $newsletters)
                <tr>
                    <td>{{ $newsletters->name }}</td>
                    <td>{{ $newsletters->email }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['newsletters.destroy', $newsletters->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('newsletters.show', [$newsletters->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
