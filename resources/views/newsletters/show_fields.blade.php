<div class="row">
    <!-- name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            <p>{{ $newsletters->name }}</p>
        </div>
    </div>
    <!-- email At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $newsletters->email }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $newsletters->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $newsletters->updated_at }}</p>
        </div>
    </div>
</div>
