<div class="sidebar-wrapper active">
    <div class="sidebar-header">
        <div class="d-flex justify-content-between">
            <div class="logo">
                <a href="{{url('/') }}"><img src="{{asset('assets/images/logo.jpg')}}" alt="Logo" srcset=""></a>
            </div>
            <div class="toggler">
                <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
            </div>
        </div>
    </div>
    <div class="sidebar-menu">
        <ul class="menu">
            <li class="sidebar-item {{ Request::is('home*') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            {{-- <li class="sidebar-item {{ Request::is('medicalSpecialties*') ? 'active' : '' }}">
                <a href="{{ route('medicalSpecialties.index') }}" class='sidebar-link'>
                    <i class="bi bi-stack"></i>
                    <span>Nos Services</span>
                </a>
            </li> --}}
            <li class="sidebar-item {{ Request::is('appointments*') ? 'active' : '' }}">
                <a href="{{ route('appointments.index') }}" class='sidebar-link'>
                    <i class="bi bi-collection-fill"></i>
                    <span>Les Rendez-Vous</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('blogs*') ? 'active' : '' }}">
                <a href="{{ route('blogs.index') }}" class='sidebar-link'>
                    <i class="bi bi-grid-1x2-fill"></i>
                    <span>Le Blog</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('medicalCheckups*') ? 'active' : '' }}">
                <a href="{{ route('medicalCheckups.index') }}" class='sidebar-link'>
                    <i class="bi bi-hexagon-fill"></i>
                    <span>Les Types d'Exemens</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('newsletters*') ? 'active' : '' }}">
                <a href="{{ route('newsletters.index') }}" class='sidebar-link'>
                    <i class="bi bi-envelope-open-fill"></i>
                    <span>La NewsLetter</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('videoStores*') ? 'active' : '' }}">
                <a href="{{ route('videoStores.index') }}" class='sidebar-link'>
                    <i class="bi bi-people-fill"></i>
                    <span>Notre Vidéothèque</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('testimonials*') ? 'active' : '' }}">
                <a href="{{ route('testimonials.index') }}" class='sidebar-link'>
                    <i class="bi bi-pen-fill"></i>
                    <span>Les Témoignages</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('slides*') ? 'active' : '' }}">
                <a href="{{ route('slides.index') }}" class='sidebar-link'>
                    <i class="bi bi-grid-1x2-fill"></i>
                    <span>Gestion du Slide</span>
                </a>
            </li>
        </ul>
    </div>
    <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
