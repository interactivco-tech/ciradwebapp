<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénoms</th>
                <th>Téléphone</th>
                <th>Examens Souhaité</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($appointments as $appointment)
                <tr>

                    <td>{{ $appointment->last_name }}</td>
                    <td>{{ $appointment->first_name }}</td>
                    <td>{{ $appointment->phone }}</td>
                    <td>{{ $appointment->getMedicalCheckupsAttribute()->title ?? '---' }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['appointments.destroy', $appointment->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('appointments.show', [$appointment->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {{-- <button type="button" class="btn btn-warning"><a href="{{ route('appointments.edit', [$appointment->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button> --}}
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
