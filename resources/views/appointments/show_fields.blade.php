<div class="row">
    <!-- last_name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('last_name', 'Noms:') !!}
            <p>{{ $appointment->last_name }}</p>
        </div>
    </div>
    <!-- first_name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('first_name', 'Prénoms:') !!}
            <p>{{ $appointment->first_name }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- phone At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('sexe', 'Sexe:') !!}
            <p>{{ $appointment->sexe }}</p>
        </div>
    </div>
    <!-- phone At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('phone', 'Téléphone:') !!}
            <p>{{ $appointment->phone }}</p>
        </div>
    </div>
    <!-- email At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $appointment->email }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- birthdate At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('birthdate', 'Date de Naissance:') !!}
            <p>{{ $appointment->birthdate }}</p>
        </div>
    </div>
    <!-- location_residence At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('location_residence', 'Lieu de Résidence:') !!}
            <p>{{ $appointment->location_residence }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- received_by_cirad At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('received_by_cirad', 'Avez-vous déjà été reçu par le Cirad?:') !!}
            <p>{{ $appointment->received_by_cirad }}</p>
        </div>
    </div>
    <!-- medical_checkup_id At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('medical_checkup_id', 'Examen Médical souhaité:') !!}
            <p>{{ $appointment->getMedicalCheckupsAttribute()->title ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- appointment_date At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('appointment_date', 'Date du Rendez-vous:') !!}
            <p>{{ $appointment->appointment_date }}</p>
        </div>
    </div>
    <!-- appointment_time At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('appointment_time', 'Heure du Rendez-vous:') !!}
            <p>{{ $appointment->appointment_time }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $appointment->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $appointment->updated_at }}</p>
        </div>
    </div>
</div>
