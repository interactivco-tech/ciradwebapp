<!-- Title Field -->
<div class="col-md-12 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre :') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre']) !!}
    </div>
</div>

<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Message du Client:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-warning me-1 mb-1']) !!}
    <a href="{{ route('testimonials.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
