<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom du client</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($testimonials as $testimonials)
                <tr>
                    <td>{{ $testimonials->title }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['testimonials.destroy', $testimonials->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('testimonials.show', [$testimonials->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('testimonials.edit', [$testimonials->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
