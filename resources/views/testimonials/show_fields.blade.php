
<div class="row mt-3">
    {{-- <!-- Customer_name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('customer_name', 'Nom du Client:') !!}
            <p>{{ $testimonials->customer_name }}</p>
        </div>
    </div> --}}
    <!-- Title At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Slide:') !!}
            <p>{{ $testimonials->title }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Message du Cient:') !!}
            <p>{!! $testimonials->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $testimonials->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $testimonials->updated_at }}</p>
        </div>
    </div>
</div>
