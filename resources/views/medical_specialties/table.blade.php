<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Image</th>
                <th>Nom du Service</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($medicalSpecialties as $medicalSpecialties)
                <tr>

                    <td><img src="{{ asset('/storage/images/' .$medicalSpecialties->image) }}" alt="{{ $medicalSpecialties->title }}" width="90"></td>
                    <td>{{ $medicalSpecialties->title }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['medicalSpecialties.destroy', $medicalSpecialties->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('medicalSpecialties.show', [$medicalSpecialties->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('medicalSpecialties.edit', [$medicalSpecialties->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
