<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Service:') !!}
            <p>{{ $medicalSpecialties->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $medicalSpecialties->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description du Service:') !!}
            <p>{!! $medicalSpecialties->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $medicalSpecialties->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="col-12 col-md-12 order-md-1 order-last">
    <h3 style="color: black !important">Précaution Avant l'Examen</h3>
</div>

<div class="row mt-3">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Service:') !!}
            <p>{{ $medicalSpecialties->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $medicalSpecialties->slug }}</p>
        </div>
    </div>
</div>

<div class="col-12 col-md-12 order-md-1 order-last">
    <h3 style="color: black !important">Précaution Durant l'Examen</h3>
</div>

<div class="row mt-3">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Service:') !!}
            <p>{{ $medicalSpecialties->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $medicalSpecialties->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $medicalSpecialties->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $medicalSpecialties->updated_at }}</p>
        </div>
    </div>
</div>
