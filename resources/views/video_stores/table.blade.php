<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Sous-Titre</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($videoStores as $videoStores)
                <tr>

                    <td>{{ $videoStores->title }}</td>
                    <td>{{ $videoStores->subtitle }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['videoStores.destroy', $videoStores->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('videoStores.show', [$videoStores->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('videoStores.edit', [$videoStores->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
