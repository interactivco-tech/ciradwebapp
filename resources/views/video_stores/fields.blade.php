<!-- Title Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre :') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre']) !!}
    </div>
</div>

<!-- SubTitle Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('subtitle', 'Sous-Titre:') !!}
        {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => 'Sous-Titre']) !!}
    </div>
</div>

<!-- link_youtube Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('link_youtube', 'Lien Youtube:') !!}
        {!! Form::text('link_youtube', null, ['class' => 'form-control', 'placeholder' => 'Lien Youtube']) !!}
        <small>Exemple: pour le lien suivant https://www.youtube.com/watch?v=mmq5zZfmIws vous devrez enregistrer l'id de la video qui est <strong>"mmq5zZfmIws"</strong></small>
    </div>
</div>

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-danger me-1 mb-1']) !!}
    <a href="{{ route('videoStores.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
