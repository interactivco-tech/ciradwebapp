<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre de la Vidéo:') !!}
            <p>{{ $videoStores->title }}</p>
        </div>
    </div>
    <!-- Subtitle At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            <p>{{ $videoStores->subtitle }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- link_youtube At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('link_youtube', 'Lien de la Vidéo:') !!}
            <p>{{ $videoStores->link_youtube }}</p>
            <iframe class="embed-responsive-item" src="{{ $videoStores->link_youtube }}"></iframe>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $videoStores->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $videoStores->updated_at }}</p>
        </div>
    </div>
</div>
