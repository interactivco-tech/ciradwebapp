<?php

namespace App\Repositories;

use App\Models\PrecautioDuringExams;
use App\Repositories\BaseRepository;

/**
 * Class PrecautioDuringExamsRepository
 * @package App\Repositories
 * @version June 9, 2021, 12:03 pm UTC
*/

class PrecautioDuringExamsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PrecautioDuringExams::class;
    }
}
