<?php

namespace App\Repositories;

use App\Models\Testimonials;
use App\Repositories\BaseRepository;

/**
 * Class TestimonialsRepository
 * @package App\Repositories
 * @version June 9, 2021, 11:56 am UTC
*/

class TestimonialsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Testimonials::class;
    }
}
