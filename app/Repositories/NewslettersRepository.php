<?php

namespace App\Repositories;

use App\Models\Newsletters;
use App\Repositories\BaseRepository;

/**
 * Class NewslettersRepository
 * @package App\Repositories
 * @version June 9, 2021, 11:54 am UTC
*/

class NewslettersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Newsletters::class;
    }
}
