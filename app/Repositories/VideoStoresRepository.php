<?php

namespace App\Repositories;

use App\Models\VideoStores;
use App\Repositories\BaseRepository;

/**
 * Class VideoStoresRepository
 * @package App\Repositories
 * @version June 9, 2021, 11:58 am UTC
*/

class VideoStoresRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VideoStores::class;
    }
}
