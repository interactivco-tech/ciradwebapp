<?php

namespace App\Repositories;

use App\Models\PrecautionBeforExams;
use App\Repositories\BaseRepository;

/**
 * Class PrecautionBeforExamsRepository
 * @package App\Repositories
 * @version June 9, 2021, 12:02 pm UTC
*/

class PrecautionBeforExamsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PrecautionBeforExams::class;
    }
}
