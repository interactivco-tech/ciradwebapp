<?php

namespace App\Repositories;

use App\Models\MedicalCheckups;
use App\Repositories\BaseRepository;

/**
 * Class MedicalCheckupsRepository
 * @package App\Repositories
 * @version June 9, 2021, 11:57 am UTC
*/

class MedicalCheckupsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicalCheckups::class;
    }
}
