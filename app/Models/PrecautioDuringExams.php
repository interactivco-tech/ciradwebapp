<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PrecautioDuringExams
 * @package App\Models
 * @version June 9, 2021, 12:03 pm UTC
 *
 */
class PrecautioDuringExams extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'precautio_during_exams';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'advice1',
        'advice2',
        'advice3',
        'advice4',
        'message',
        'description',
        'medical_specialtie_id'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'advice1' => 'string',
        'advice2' => 'string',
        'advice3' => 'string',
        'advice4' => 'string',
        'medical_specialtie_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
