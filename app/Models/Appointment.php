<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Appointment
 * @package App\Models
 * @version June 9, 2021, 11:53 am UTC
 *
 */
class Appointment extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'appointments';


    protected $dates = ['deleted_at'];

    protected $appends = ['MedicalCheckups'];



    public $fillable = [
        'last_name',
        'first_name',
        'phone',
        'email',
        'sexe',
        'location_residence',
        'received_by_cirad',
        'medical_checkup_id',
        'appointment_time',
        'appointment_date',
        'birthdate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'last_name' => 'string',
        'first_name' => 'string',
        'sexe' => 'string',
        'phone' => 'string',
        'medical_checkup_id' => 'integer',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function MedicalCheckups()
    {
        return $this->belongsTo(MedicalCheckups::class);
    }

    public function getMedicalCheckupsAttribute(){

        $medicalCheckups = MedicalCheckups::find($this->medical_checkup_id);

        return $medicalCheckups;
    }

}
