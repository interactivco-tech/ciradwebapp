<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class VideoStores
 * @package App\Models
 * @version June 9, 2021, 11:58 am UTC
 *
 */
class VideoStores extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'video_stores';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'subtitle',
        'link_youtube'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'link_youtube' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
