<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Testimonials
 * @package App\Models
 * @version June 9, 2021, 11:56 am UTC
 *
 */
class Testimonials extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'testimonials';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_name',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_name' => 'string',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
