<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Newsletters
 * @package App\Models
 * @version June 9, 2021, 11:54 am UTC
 *
 */
class Newsletters extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'newsletters';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
