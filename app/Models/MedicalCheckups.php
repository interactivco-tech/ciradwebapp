<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MedicalCheckups
 * @package App\Models
 * @version June 9, 2021, 11:57 am UTC
 *
 */
class MedicalCheckups extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'medical_checkups';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getAppointmentAttribute()
    {
        return $this->hasMany(Appointment::class);
    }

}
