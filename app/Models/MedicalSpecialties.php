<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MedicalSpecialties
 * @package App\Models
 * @version June 9, 2021, 11:52 am UTC
 *
 */
class MedicalSpecialties extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'medical_specialties';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'image',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
