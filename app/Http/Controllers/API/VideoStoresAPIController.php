<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVideoStoresAPIRequest;
use App\Http\Requests\API\UpdateVideoStoresAPIRequest;
use App\Models\VideoStores;
use App\Repositories\VideoStoresRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class VideoStoresController
 * @package App\Http\Controllers\API
 */

class VideoStoresAPIController extends AppBaseController
{
    /** @var  VideoStoresRepository */
    private $videoStoresRepository;

    public function __construct(VideoStoresRepository $videoStoresRepo)
    {
        $this->videoStoresRepository = $videoStoresRepo;
    }

    /**
     * Display a listing of the VideoStores.
     * GET|HEAD /videoStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $videoStores = $this->videoStoresRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($videoStores->toArray(), 'Video Stores retrieved successfully');
    }

    /**
     * Store a newly created VideoStores in storage.
     * POST /videoStores
     *
     * @param CreateVideoStoresAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVideoStoresAPIRequest $request)
    {
        $input = $request->all();

        $videoStores = $this->videoStoresRepository->create($input);

        return $this->sendResponse($videoStores->toArray(), 'Video Stores saved successfully');
    }

    /**
     * Display the specified VideoStores.
     * GET|HEAD /videoStores/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VideoStores $videoStores */
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            return $this->sendError('Video Stores not found');
        }

        return $this->sendResponse($videoStores->toArray(), 'Video Stores retrieved successfully');
    }

    /**
     * Update the specified VideoStores in storage.
     * PUT/PATCH /videoStores/{id}
     *
     * @param int $id
     * @param UpdateVideoStoresAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVideoStoresAPIRequest $request)
    {
        $input = $request->all();

        /** @var VideoStores $videoStores */
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            return $this->sendError('Video Stores not found');
        }

        $videoStores = $this->videoStoresRepository->update($input, $id);

        return $this->sendResponse($videoStores->toArray(), 'VideoStores updated successfully');
    }

    /**
     * Remove the specified VideoStores from storage.
     * DELETE /videoStores/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VideoStores $videoStores */
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            return $this->sendError('Video Stores not found');
        }

        $videoStores->delete();

        return $this->sendSuccess('Video Stores deleted successfully');
    }
}
