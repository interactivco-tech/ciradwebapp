<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePrecautionBeforExamsAPIRequest;
use App\Http\Requests\API\UpdatePrecautionBeforExamsAPIRequest;
use App\Models\PrecautionBeforExams;
use App\Repositories\PrecautionBeforExamsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PrecautionBeforExamsController
 * @package App\Http\Controllers\API
 */

class PrecautionBeforExamsAPIController extends AppBaseController
{
    /** @var  PrecautionBeforExamsRepository */
    private $precautionBeforExamsRepository;

    public function __construct(PrecautionBeforExamsRepository $precautionBeforExamsRepo)
    {
        $this->precautionBeforExamsRepository = $precautionBeforExamsRepo;
    }

    /**
     * Display a listing of the PrecautionBeforExams.
     * GET|HEAD /precautionBeforExams
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $precautionBeforExams = $this->precautionBeforExamsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($precautionBeforExams->toArray(), 'Precaution Befor Exams retrieved successfully');
    }

    /**
     * Store a newly created PrecautionBeforExams in storage.
     * POST /precautionBeforExams
     *
     * @param CreatePrecautionBeforExamsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePrecautionBeforExamsAPIRequest $request)
    {
        $input = $request->all();

        $precautionBeforExams = $this->precautionBeforExamsRepository->create($input);

        return $this->sendResponse($precautionBeforExams->toArray(), 'Precaution Befor Exams saved successfully');
    }

    /**
     * Display the specified PrecautionBeforExams.
     * GET|HEAD /precautionBeforExams/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PrecautionBeforExams $precautionBeforExams */
        $precautionBeforExams = $this->precautionBeforExamsRepository->find($id);

        if (empty($precautionBeforExams)) {
            return $this->sendError('Precaution Befor Exams not found');
        }

        return $this->sendResponse($precautionBeforExams->toArray(), 'Precaution Befor Exams retrieved successfully');
    }

    /**
     * Update the specified PrecautionBeforExams in storage.
     * PUT/PATCH /precautionBeforExams/{id}
     *
     * @param int $id
     * @param UpdatePrecautionBeforExamsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePrecautionBeforExamsAPIRequest $request)
    {
        $input = $request->all();

        /** @var PrecautionBeforExams $precautionBeforExams */
        $precautionBeforExams = $this->precautionBeforExamsRepository->find($id);

        if (empty($precautionBeforExams)) {
            return $this->sendError('Precaution Befor Exams not found');
        }

        $precautionBeforExams = $this->precautionBeforExamsRepository->update($input, $id);

        return $this->sendResponse($precautionBeforExams->toArray(), 'PrecautionBeforExams updated successfully');
    }

    /**
     * Remove the specified PrecautionBeforExams from storage.
     * DELETE /precautionBeforExams/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PrecautionBeforExams $precautionBeforExams */
        $precautionBeforExams = $this->precautionBeforExamsRepository->find($id);

        if (empty($precautionBeforExams)) {
            return $this->sendError('Precaution Befor Exams not found');
        }

        $precautionBeforExams->delete();

        return $this->sendSuccess('Precaution Befor Exams deleted successfully');
    }
}
