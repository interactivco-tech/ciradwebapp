<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicalCheckupsAPIRequest;
use App\Http\Requests\API\UpdateMedicalCheckupsAPIRequest;
use App\Models\MedicalCheckups;
use App\Repositories\MedicalCheckupsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicalCheckupsController
 * @package App\Http\Controllers\API
 */

class MedicalCheckupsAPIController extends AppBaseController
{
    /** @var  MedicalCheckupsRepository */
    private $medicalCheckupsRepository;

    public function __construct(MedicalCheckupsRepository $medicalCheckupsRepo)
    {
        $this->medicalCheckupsRepository = $medicalCheckupsRepo;
    }

    /**
     * Display a listing of the MedicalCheckups.
     * GET|HEAD /medicalCheckups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $medicalCheckups = $this->medicalCheckupsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicalCheckups->toArray(), 'Medical Checkups retrieved successfully');
    }

    /**
     * Store a newly created MedicalCheckups in storage.
     * POST /medicalCheckups
     *
     * @param CreateMedicalCheckupsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalCheckupsAPIRequest $request)
    {
        $input = $request->all();

        $medicalCheckups = $this->medicalCheckupsRepository->create($input);

        return $this->sendResponse($medicalCheckups->toArray(), 'Medical Checkups saved successfully');
    }

    /**
     * Display the specified MedicalCheckups.
     * GET|HEAD /medicalCheckups/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicalCheckups $medicalCheckups */
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            return $this->sendError('Medical Checkups not found');
        }

        return $this->sendResponse($medicalCheckups->toArray(), 'Medical Checkups retrieved successfully');
    }

    /**
     * Update the specified MedicalCheckups in storage.
     * PUT/PATCH /medicalCheckups/{id}
     *
     * @param int $id
     * @param UpdateMedicalCheckupsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalCheckupsAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicalCheckups $medicalCheckups */
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            return $this->sendError('Medical Checkups not found');
        }

        $medicalCheckups = $this->medicalCheckupsRepository->update($input, $id);

        return $this->sendResponse($medicalCheckups->toArray(), 'MedicalCheckups updated successfully');
    }

    /**
     * Remove the specified MedicalCheckups from storage.
     * DELETE /medicalCheckups/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicalCheckups $medicalCheckups */
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            return $this->sendError('Medical Checkups not found');
        }

        $medicalCheckups->delete();

        return $this->sendSuccess('Medical Checkups deleted successfully');
    }
}
