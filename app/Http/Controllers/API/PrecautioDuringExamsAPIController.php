<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePrecautioDuringExamsAPIRequest;
use App\Http\Requests\API\UpdatePrecautioDuringExamsAPIRequest;
use App\Models\PrecautioDuringExams;
use App\Repositories\PrecautioDuringExamsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PrecautioDuringExamsController
 * @package App\Http\Controllers\API
 */

class PrecautioDuringExamsAPIController extends AppBaseController
{
    /** @var  PrecautioDuringExamsRepository */
    private $precautioDuringExamsRepository;

    public function __construct(PrecautioDuringExamsRepository $precautioDuringExamsRepo)
    {
        $this->precautioDuringExamsRepository = $precautioDuringExamsRepo;
    }

    /**
     * Display a listing of the PrecautioDuringExams.
     * GET|HEAD /precautioDuringExams
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $precautioDuringExams = $this->precautioDuringExamsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($precautioDuringExams->toArray(), 'Precautio During Exams retrieved successfully');
    }

    /**
     * Store a newly created PrecautioDuringExams in storage.
     * POST /precautioDuringExams
     *
     * @param CreatePrecautioDuringExamsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePrecautioDuringExamsAPIRequest $request)
    {
        $input = $request->all();

        $precautioDuringExams = $this->precautioDuringExamsRepository->create($input);

        return $this->sendResponse($precautioDuringExams->toArray(), 'Precautio During Exams saved successfully');
    }

    /**
     * Display the specified PrecautioDuringExams.
     * GET|HEAD /precautioDuringExams/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PrecautioDuringExams $precautioDuringExams */
        $precautioDuringExams = $this->precautioDuringExamsRepository->find($id);

        if (empty($precautioDuringExams)) {
            return $this->sendError('Precautio During Exams not found');
        }

        return $this->sendResponse($precautioDuringExams->toArray(), 'Precautio During Exams retrieved successfully');
    }

    /**
     * Update the specified PrecautioDuringExams in storage.
     * PUT/PATCH /precautioDuringExams/{id}
     *
     * @param int $id
     * @param UpdatePrecautioDuringExamsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePrecautioDuringExamsAPIRequest $request)
    {
        $input = $request->all();

        /** @var PrecautioDuringExams $precautioDuringExams */
        $precautioDuringExams = $this->precautioDuringExamsRepository->find($id);

        if (empty($precautioDuringExams)) {
            return $this->sendError('Precautio During Exams not found');
        }

        $precautioDuringExams = $this->precautioDuringExamsRepository->update($input, $id);

        return $this->sendResponse($precautioDuringExams->toArray(), 'PrecautioDuringExams updated successfully');
    }

    /**
     * Remove the specified PrecautioDuringExams from storage.
     * DELETE /precautioDuringExams/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PrecautioDuringExams $precautioDuringExams */
        $precautioDuringExams = $this->precautioDuringExamsRepository->find($id);

        if (empty($precautioDuringExams)) {
            return $this->sendError('Precautio During Exams not found');
        }

        $precautioDuringExams->delete();

        return $this->sendSuccess('Precautio During Exams deleted successfully');
    }
}
