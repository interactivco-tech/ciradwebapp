<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNewslettersAPIRequest;
use App\Http\Requests\API\UpdateNewslettersAPIRequest;
use App\Models\Newsletters;
use App\Repositories\NewslettersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NewslettersController
 * @package App\Http\Controllers\API
 */

class NewslettersAPIController extends AppBaseController
{
    /** @var  NewslettersRepository */
    private $newslettersRepository;

    public function __construct(NewslettersRepository $newslettersRepo)
    {
        $this->newslettersRepository = $newslettersRepo;
    }

    /**
     * Display a listing of the Newsletters.
     * GET|HEAD /newsletters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $newsletters = $this->newslettersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($newsletters->toArray(), 'Newsletters retrieved successfully');
    }

    /**
     * Store a newly created Newsletters in storage.
     * POST /newsletters
     *
     * @param CreateNewslettersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNewslettersAPIRequest $request)
    {
        $input = $request->all();

        $newsletters = $this->newslettersRepository->create($input);

        return $this->sendResponse($newsletters->toArray(), 'Newsletters saved successfully');
    }

    /**
     * Display the specified Newsletters.
     * GET|HEAD /newsletters/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Newsletters $newsletters */
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            return $this->sendError('Newsletters not found');
        }

        return $this->sendResponse($newsletters->toArray(), 'Newsletters retrieved successfully');
    }

    /**
     * Update the specified Newsletters in storage.
     * PUT/PATCH /newsletters/{id}
     *
     * @param int $id
     * @param UpdateNewslettersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewslettersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Newsletters $newsletters */
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            return $this->sendError('Newsletters not found');
        }

        $newsletters = $this->newslettersRepository->update($input, $id);

        return $this->sendResponse($newsletters->toArray(), 'Newsletters updated successfully');
    }

    /**
     * Remove the specified Newsletters from storage.
     * DELETE /newsletters/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Newsletters $newsletters */
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            return $this->sendError('Newsletters not found');
        }

        $newsletters->delete();

        return $this->sendSuccess('Newsletters deleted successfully');
    }
}
