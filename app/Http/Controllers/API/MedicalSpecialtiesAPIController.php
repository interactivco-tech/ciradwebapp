<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicalSpecialtiesAPIRequest;
use App\Http\Requests\API\UpdateMedicalSpecialtiesAPIRequest;
use App\Models\MedicalSpecialties;
use App\Repositories\MedicalSpecialtiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicalSpecialtiesController
 * @package App\Http\Controllers\API
 */

class MedicalSpecialtiesAPIController extends AppBaseController
{
    /** @var  MedicalSpecialtiesRepository */
    private $medicalSpecialtiesRepository;

    public function __construct(MedicalSpecialtiesRepository $medicalSpecialtiesRepo)
    {
        $this->medicalSpecialtiesRepository = $medicalSpecialtiesRepo;
    }

    /**
     * Display a listing of the MedicalSpecialties.
     * GET|HEAD /medicalSpecialties
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $medicalSpecialties = $this->medicalSpecialtiesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicalSpecialties->toArray(), 'Medical Specialties retrieved successfully');
    }

    /**
     * Store a newly created MedicalSpecialties in storage.
     * POST /medicalSpecialties
     *
     * @param CreateMedicalSpecialtiesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalSpecialtiesAPIRequest $request)
    {
        $input = $request->all();

        $medicalSpecialties = $this->medicalSpecialtiesRepository->create($input);

        return $this->sendResponse($medicalSpecialties->toArray(), 'Medical Specialties saved successfully');
    }

    /**
     * Display the specified MedicalSpecialties.
     * GET|HEAD /medicalSpecialties/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicalSpecialties $medicalSpecialties */
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            return $this->sendError('Medical Specialties not found');
        }

        return $this->sendResponse($medicalSpecialties->toArray(), 'Medical Specialties retrieved successfully');
    }

    /**
     * Update the specified MedicalSpecialties in storage.
     * PUT/PATCH /medicalSpecialties/{id}
     *
     * @param int $id
     * @param UpdateMedicalSpecialtiesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalSpecialtiesAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicalSpecialties $medicalSpecialties */
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            return $this->sendError('Medical Specialties not found');
        }

        $medicalSpecialties = $this->medicalSpecialtiesRepository->update($input, $id);

        return $this->sendResponse($medicalSpecialties->toArray(), 'MedicalSpecialties updated successfully');
    }

    /**
     * Remove the specified MedicalSpecialties from storage.
     * DELETE /medicalSpecialties/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicalSpecialties $medicalSpecialties */
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            return $this->sendError('Medical Specialties not found');
        }

        $medicalSpecialties->delete();

        return $this->sendSuccess('Medical Specialties deleted successfully');
    }
}
