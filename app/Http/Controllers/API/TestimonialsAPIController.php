<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTestimonialsAPIRequest;
use App\Http\Requests\API\UpdateTestimonialsAPIRequest;
use App\Models\Testimonials;
use App\Repositories\TestimonialsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TestimonialsController
 * @package App\Http\Controllers\API
 */

class TestimonialsAPIController extends AppBaseController
{
    /** @var  TestimonialsRepository */
    private $testimonialsRepository;

    public function __construct(TestimonialsRepository $testimonialsRepo)
    {
        $this->testimonialsRepository = $testimonialsRepo;
    }

    /**
     * Display a listing of the Testimonials.
     * GET|HEAD /testimonials
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $testimonials = $this->testimonialsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($testimonials->toArray(), 'Testimonials retrieved successfully');
    }

    /**
     * Store a newly created Testimonials in storage.
     * POST /testimonials
     *
     * @param CreateTestimonialsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTestimonialsAPIRequest $request)
    {
        $input = $request->all();

        $testimonials = $this->testimonialsRepository->create($input);

        return $this->sendResponse($testimonials->toArray(), 'Testimonials saved successfully');
    }

    /**
     * Display the specified Testimonials.
     * GET|HEAD /testimonials/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Testimonials $testimonials */
        $testimonials = $this->testimonialsRepository->find($id);

        if (empty($testimonials)) {
            return $this->sendError('Testimonials not found');
        }

        return $this->sendResponse($testimonials->toArray(), 'Testimonials retrieved successfully');
    }

    /**
     * Update the specified Testimonials in storage.
     * PUT/PATCH /testimonials/{id}
     *
     * @param int $id
     * @param UpdateTestimonialsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTestimonialsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Testimonials $testimonials */
        $testimonials = $this->testimonialsRepository->find($id);

        if (empty($testimonials)) {
            return $this->sendError('Testimonials not found');
        }

        $testimonials = $this->testimonialsRepository->update($input, $id);

        return $this->sendResponse($testimonials->toArray(), 'Testimonials updated successfully');
    }

    /**
     * Remove the specified Testimonials from storage.
     * DELETE /testimonials/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Testimonials $testimonials */
        $testimonials = $this->testimonialsRepository->find($id);

        if (empty($testimonials)) {
            return $this->sendError('Testimonials not found');
        }

        $testimonials->delete();

        return $this->sendSuccess('Testimonials deleted successfully');
    }
}
