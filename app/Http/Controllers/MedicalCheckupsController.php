<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMedicalCheckupsRequest;
use App\Http\Requests\UpdateMedicalCheckupsRequest;
use App\Repositories\MedicalCheckupsRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\MedicalCheckups;
use Illuminate\Http\Request;
use Flash;
use Response;

class MedicalCheckupsController extends AppBaseController
{
    /** @var  MedicalCheckupsRepository */
    private $medicalCheckupsRepository;

    public function __construct(MedicalCheckupsRepository $medicalCheckupsRepo)
    {
        $this->medicalCheckupsRepository = $medicalCheckupsRepo;
    }

    /**
     * Display a listing of the MedicalCheckups.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $medicalCheckups = $this->medicalCheckupsRepository->all();
        $medicalCheckups = MedicalCheckups::orderBy('id', 'desc')->paginate(10);

        return view('medical_checkups.index')
            ->with('medicalCheckups', $medicalCheckups);
    }

    /**
     * Show the form for creating a new MedicalCheckups.
     *
     * @return Response
     */
    public function create()
    {
        return view('medical_checkups.create');
    }

    /**
     * Store a newly created MedicalCheckups in storage.
     *
     * @param CreateMedicalCheckupsRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalCheckupsRequest $request)
    {
        $input = $request->all();

        $medicalCheckups = $this->medicalCheckupsRepository->create($input);

        Flash::success('Medical Checkups saved successfully.');

        return redirect(route('medicalCheckups.index'));
    }

    /**
     * Display the specified MedicalCheckups.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            Flash::error('Medical Checkups not found');

            return redirect(route('medicalCheckups.index'));
        }

        return view('medical_checkups.show')->with('medicalCheckups', $medicalCheckups);
    }

    /**
     * Show the form for editing the specified MedicalCheckups.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            Flash::error('Medical Checkups not found');

            return redirect(route('medicalCheckups.index'));
        }

        return view('medical_checkups.edit')->with('medicalCheckups', $medicalCheckups);
    }

    /**
     * Update the specified MedicalCheckups in storage.
     *
     * @param int $id
     * @param UpdateMedicalCheckupsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalCheckupsRequest $request)
    {
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            Flash::error('Medical Checkups not found');

            return redirect(route('medicalCheckups.index'));
        }

        $medicalCheckups = $this->medicalCheckupsRepository->update($request->all(), $id);

        Flash::success('Medical Checkups updated successfully.');

        return redirect(route('medicalCheckups.index'));
    }

    /**
     * Remove the specified MedicalCheckups from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medicalCheckups = $this->medicalCheckupsRepository->find($id);

        if (empty($medicalCheckups)) {
            Flash::error('Medical Checkups not found');

            return redirect(route('medicalCheckups.index'));
        }

        $this->medicalCheckupsRepository->delete($id);

        Flash::success('Medical Checkups deleted successfully.');

        return redirect(route('medicalCheckups.index'));
    }
}
