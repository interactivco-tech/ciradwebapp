<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMedicalSpecialtiesRequest;
use App\Http\Requests\UpdateMedicalSpecialtiesRequest;
use App\Repositories\MedicalSpecialtiesRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\MedicalSpecialties;
use Illuminate\Http\Request;
use Flash;
use Response;

class MedicalSpecialtiesController extends AppBaseController
{
    /** @var  MedicalSpecialtiesRepository */
    private $medicalSpecialtiesRepository;

    public function __construct(MedicalSpecialtiesRepository $medicalSpecialtiesRepo)
    {
        $this->medicalSpecialtiesRepository = $medicalSpecialtiesRepo;
    }

    /**
     * Display a listing of the MedicalSpecialties.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $medicalSpecialties = $this->medicalSpecialtiesRepository->all();
        $medicalSpecialties = MedicalSpecialties::orderBy('id', 'desc')->paginate(10);

        return view('medical_specialties.index')
            ->with('medicalSpecialties', $medicalSpecialties);
    }

    /**
     * Show the form for creating a new MedicalSpecialties.
     *
     * @return Response
     */
    public function create()
    {
        return view('medical_specialties.create');
    }

    /**
     * Store a newly created MedicalSpecialties in storage.
     *
     * @param CreateMedicalSpecialtiesRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalSpecialtiesRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        $medicalSpecialties = $this->medicalSpecialtiesRepository->create($input);

        Flash::success('Medical Specialties saved successfully.');

        return redirect(route('medicalSpecialties.index'));
    }

    /**
     * Display the specified MedicalSpecialties.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            Flash::error('Medical Specialties not found');

            return redirect(route('medicalSpecialties.index'));
        }

        return view('medical_specialties.show')->with('medicalSpecialties', $medicalSpecialties);
    }

    /**
     * Show the form for editing the specified MedicalSpecialties.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            Flash::error('Medical Specialties not found');

            return redirect(route('medicalSpecialties.index'));
        }

        return view('medical_specialties.edit')->with('medicalSpecialties', $medicalSpecialties);
    }

    /**
     * Update the specified MedicalSpecialties in storage.
     *
     * @param int $id
     * @param UpdateMedicalSpecialtiesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalSpecialtiesRequest $request)
    {
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            Flash::error('Medical Specialties not found');

            return redirect(route('medicalSpecialties.index'));
        }

        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        $medicalSpecialties = $this->medicalSpecialtiesRepository->update($request->all(), $id);

        Flash::success('Medical Specialties updated successfully.');

        return redirect(route('medicalSpecialties.index'));
    }

    /**
     * Remove the specified MedicalSpecialties from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medicalSpecialties = $this->medicalSpecialtiesRepository->find($id);

        if (empty($medicalSpecialties)) {
            Flash::error('Medical Specialties not found');

            return redirect(route('medicalSpecialties.index'));
        }

        $this->medicalSpecialtiesRepository->delete($id);

        Flash::success('Medical Specialties deleted successfully.');

        return redirect(route('medicalSpecialties.index'));
    }
}
