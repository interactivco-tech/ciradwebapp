<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVideoStoresRequest;
use App\Http\Requests\UpdateVideoStoresRequest;
use App\Repositories\VideoStoresRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\VideoStores;
use Illuminate\Http\Request;
use Flash;
use Response;

class VideoStoresController extends AppBaseController
{
    /** @var  VideoStoresRepository */
    private $videoStoresRepository;

    public function __construct(VideoStoresRepository $videoStoresRepo)
    {
        $this->videoStoresRepository = $videoStoresRepo;
    }

    /**
     * Display a listing of the VideoStores.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $videoStores = VideoStores::orderBy('id', 'desc')->paginate(10);
        // $videoStores = $this->videoStoresRepository->all();

        return view('video_stores.index')
            ->with('videoStores', $videoStores);
    }

    /**
     * Show the form for creating a new VideoStores.
     *
     * @return Response
     */
    public function create()
    {
        return view('video_stores.create');
    }

    /**
     * Store a newly created VideoStores in storage.
     *
     * @param CreateVideoStoresRequest $request
     *
     * @return Response
     */
    public function store(CreateVideoStoresRequest $request)
    {
        $input = $request->all();

        if(!is_null($input['link_youtube'])){
            $input['link_youtube'] = 'https://www.youtube.com/embed/'.$input['link_youtube'];
        }

        $videoStores = $this->videoStoresRepository->create($input);

        Flash::success('Video Stores saved successfully.');

        return redirect(route('videoStores.index'));
    }

    /**
     * Display the specified VideoStores.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            Flash::error('Video Stores not found');

            return redirect(route('videoStores.index'));
        }

        return view('video_stores.show')->with('videoStores', $videoStores);
    }

    /**
     * Show the form for editing the specified VideoStores.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            Flash::error('Video Stores not found');

            return redirect(route('videoStores.index'));
        }

        return view('video_stores.edit')->with('videoStores', $videoStores);
    }

    /**
     * Update the specified VideoStores in storage.
     *
     * @param int $id
     * @param UpdateVideoStoresRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVideoStoresRequest $request)
    {
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            Flash::error('Video Stores not found');

            return redirect(route('videoStores.index'));
        }

        $videoStores = $this->videoStoresRepository->update($request->all(), $id);

        Flash::success('Video Stores updated successfully.');

        return redirect(route('videoStores.index'));
    }

    /**
     * Remove the specified VideoStores from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $videoStores = $this->videoStoresRepository->find($id);

        if (empty($videoStores)) {
            Flash::error('Video Stores not found');

            return redirect(route('videoStores.index'));
        }

        $this->videoStoresRepository->delete($id);

        Flash::success('Video Stores deleted successfully.');

        return redirect(route('videoStores.index'));
    }
}
