<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAppointmentRequest;
use App\Http\Requests\UpdateAppointmentRequest;
use App\Repositories\AppointmentRepository;
use App\Http\Controllers\AppBaseController;
use App\Mail\Appointments;
use App\Models\Appointment;
use App\Models\MedicalCheckups;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Mail;
use Response;

class AppointmentController extends AppBaseController
{
    /** @var  AppointmentRepository */
    private $appointmentRepository;

    public function __construct(AppointmentRepository $appointmentRepo)
    {
        $this->appointmentRepository = $appointmentRepo;
    }

    /**
     * Display a listing of the Appointment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $appointments = Appointment::orderBy('id', 'desc')->paginate(10);

        // $appointments = $this->appointmentRepository->all();

        return view('appointments.index')
            ->with('appointments', $appointments);
    }

    /**
     * Show the form for creating a new Appointment.
     *
     * @return Response
     */
    public function create()
    {
        return view('appointments.create');
    }

    /**
     * Store a newly created Appointment in storage.
     *
     * @param CreateAppointmentRequest $request
     *
     * @return Response
     */
    public function store(CreateAppointmentRequest $request)
    {
        $input = $request->all();

        $appointment = $this->appointmentRepository->create($input);

        Flash::success('Appointment saved successfully.');

        return redirect(route('appointments.index'));
    }

    /**
     * Display the specified Appointment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $appointment = $this->appointmentRepository->find($id);

        if (empty($appointment)) {
            Flash::error('Appointment not found');

            return redirect(route('appointments.index'));
        }

        return view('appointments.show')->with('appointment', $appointment);
    }

    /**
     * Show the form for editing the specified Appointment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $appointment = $this->appointmentRepository->find($id);

        if (empty($appointment)) {
            Flash::error('Appointment not found');

            return redirect(route('appointments.index'));
        }

        return view('appointments.edit')->with('appointment', $appointment);
    }

    /**
     * Update the specified Appointment in storage.
     *
     * @param int $id
     * @param UpdateAppointmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAppointmentRequest $request)
    {
        $appointment = $this->appointmentRepository->find($id);

        if (empty($appointment)) {
            Flash::error('Appointment not found');

            return redirect(route('appointments.index'));
        }

        $appointment = $this->appointmentRepository->update($request->all(), $id);

        Flash::success('Appointment updated successfully.');

        return redirect(route('appointments.index'));
    }

    /**
     * Remove the specified Appointment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $appointment = $this->appointmentRepository->find($id);

        if (empty($appointment)) {
            Flash::error('Appointment not found');

            return redirect(route('appointments.index'));
        }

        $this->appointmentRepository->delete($id);

        Flash::success('Appointment deleted successfully.');

        return redirect(route('appointments.index'));
    }


    public function SendAppointment(CreateAppointmentRequest $request)
    {

        if($request->has("medical_Checkups_id")){
            if( is_null($request->get("medical_Checkups_id")) || empty($request->get("medical_Checkups_id"))){
                return $this->sendError('medical_Checkups_id is Null or Empty');
            }
        }else{
            return $this->sendError('medical_Checkups_id is Required', 400);
        }

        $medicalCheckups = MedicalCheckups::find($request->get("medical_Checkups_id"));
        // dd($medicalCheckups);

        $data = $request->validate([
            'last_name' => 'required',
            'first_name' => 'required',
            'sexe' => 'required',
            'phone' => 'required',
            'birthdate' => 'required',
            'location_residence' => 'required',
            'received_by_cirad' => 'required',
            'appointment_time' => 'required',
            'appointment_date' => 'required',
            'email' => 'required | email'
        ]);

        $appointment = new Appointment();
        $appointment->last_name = $data['last_name'];
        $appointment->first_name = $data['first_name'];
        $appointment->sexe = $data['sexe'];
        $appointment->medical_checkup_id = $medicalCheckups->id;
        $appointment->phone = $data['phone'];
        $appointment->birthdate = $data['birthdate'];
        $appointment->location_residence = $data['location_residence'];
        $appointment->received_by_cirad = $data['received_by_cirad'];
        $appointment->appointment_time = $data['appointment_time'];
        $appointment->appointment_date = $data['appointment_date'];
        $appointment->email = $data['email'];

        $appointment->save();

        session()->flash('success', "Message envoyé enregistrée avec success");

        Mail::to($appointment)
            ->send(new Appointments($appointment));

        // dd($appointment);

        // $data = [
        //     'subject' => "Nouveau message",
        //     'title' => "Vous avez reçu un nouveau message",
        //     'subtitle' => "$appointment->first_name vous a envoyé un message depuis la page de Rendez-vous.",
        // ];


        return redirect()->back();
    }
}
