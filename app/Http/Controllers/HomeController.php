<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Blog;
use App\Models\Newsletters;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $newsletters = Newsletters::all();
        $countNewsletter = count($newsletters);

        $blogs = Blog::all();
        $countBlog = count($blogs);

        $appointments = Appointment::all();
        $countAppointment = count($appointments);


        return view('home', compact('countNewsletter', 'countBlog', 'appointments', 'countAppointment'));
    }
}
