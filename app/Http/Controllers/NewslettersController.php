<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewslettersRequest;
use App\Http\Requests\UpdateNewslettersRequest;
use App\Repositories\NewslettersRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Newsletters;
use Illuminate\Http\Request;
use Flash;
use Response;

class NewslettersController extends AppBaseController
{
    /** @var  NewslettersRepository */
    private $newslettersRepository;

    public function __construct(NewslettersRepository $newslettersRepo)
    {
        $this->newslettersRepository = $newslettersRepo;
    }

    /**
     * Display a listing of the Newsletters.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $newsletters = Newsletters::orderBy('id', 'desc')->paginate(10);

        // $newsletters = $this->newslettersRepository->all();

        return view('newsletters.index')
            ->with('newsletters', $newsletters);
    }

    /**
     * Show the form for creating a new Newsletters.
     *
     * @return Response
     */
    public function create()
    {
        return view('newsletters.create');
    }

    /**
     * Store a newly created Newsletters in storage.
     *
     * @param CreateNewslettersRequest $request
     *
     * @return Response
     */
    public function store(CreateNewslettersRequest $request)
    {
        $input = $request->all();

        $newsletters = $this->newslettersRepository->create($input);

        Flash::success('Newsletters saved successfully.');

        return redirect(route('newsletters.index'));
    }

    /**
     * Display the specified Newsletters.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            Flash::error('Newsletters not found');

            return redirect(route('newsletters.index'));
        }

        return view('newsletters.show')->with('newsletters', $newsletters);
    }

    /**
     * Show the form for editing the specified Newsletters.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            Flash::error('Newsletters not found');

            return redirect(route('newsletters.index'));
        }

        return view('newsletters.edit')->with('newsletters', $newsletters);
    }

    /**
     * Update the specified Newsletters in storage.
     *
     * @param int $id
     * @param UpdateNewslettersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewslettersRequest $request)
    {
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            Flash::error('Newsletters not found');

            return redirect(route('newsletters.index'));
        }

        $newsletters = $this->newslettersRepository->update($request->all(), $id);

        Flash::success('Newsletters updated successfully.');

        return redirect(route('newsletters.index'));
    }

    /**
     * Remove the specified Newsletters from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $newsletters = $this->newslettersRepository->find($id);

        if (empty($newsletters)) {
            Flash::error('Newsletters not found');

            return redirect(route('newsletters.index'));
        }

        $this->newslettersRepository->delete($id);

        Flash::success('Newsletters deleted successfully.');

        return redirect(route('newsletters.index'));
    }

    public function addEmail(CreateNewslettersRequest $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required | email'
        ]);

        $newLetter = new Newsletters();
        $newLetter->name = $data['name'];
        $newLetter->email = $data['email'];

        $newLetter->save();

        session()->flash('success', "Message envoyé enregistrée avec success");

        return redirect()->back();
    }
}
