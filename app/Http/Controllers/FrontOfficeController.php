<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\MedicalCheckups;
use App\Models\MedicalSpecialties;
use App\Models\Slide;
use App\Models\Testimonials;
use App\Models\VideoStores;
use Illuminate\Http\Request;

class FrontOfficeController extends Controller
{
    public function indexFrontOffice()
    {
        $blogs = Blog::take(3)->get();

        $slides = Slide::take(4)->get();

        $testimonials = Testimonials::all();

        $medicalSpecialties = MedicalSpecialties::all();

        $videoStores = VideoStores::take(1)->get();

        return view('CustomerFrontOffice.index', compact("blogs", "medicalSpecialties", "slides", "testimonials", "videoStores"));
    }

    public function indexAbout()
    {
        return view('CustomerFrontOffice.about');
    }


    public function nousContacter()
    {
        return view('CustomerFrontOffice.contact');
    }

    public function appointmentPages()
    {
        $medicalCheckups = MedicalCheckups::all();

        return view('CustomerFrontOffice.appointment', compact('medicalCheckups'));
    }

    public function ourSpecialitiesPages()
    {
        return view('CustomerFrontOffice.ourSpecialities');
    }

    public function indexBlog()
    {
        $blogs = Blog::orderBy('id', 'desc')->paginate(4);

        return view('CustomerFrontOffice.blog.index', compact('blogs'));
    }

    public function showBlog($slug)
    {
        $blog = Blog::where('slug', $slug)->first();

        $blogs = Blog::take(3)->get();

        return view('CustomerFrontOffice.blog.show', compact('blog', 'blogs'));
    }

    public function irmPage()
    {
        return view('CustomerFrontOffice.speciality.irm');
    }

    public function biopsiePage()
    {
        return view('CustomerFrontOffice.speciality.biopsie');
    }

    public function echographiePage()
    {
        return view('CustomerFrontOffice.speciality.echographie');
    }

    public function scannerPage()
    {
        return view('CustomerFrontOffice.speciality.scanner');
    }

    public function mammographiePage()
    {
        return view('CustomerFrontOffice.speciality.mammographie');
    }

    public function healhCheckPage()
    {
        return view('CustomerFrontOffice.speciality.health_check');
    }

    public function medicalAnalysPage()
    {
        return view('CustomerFrontOffice.speciality.medical_analysis');
    }

    public function radiographiePage()
    {
        return view('CustomerFrontOffice.speciality.radiographie');
    }

}
